package com.gcloud.header.storage.msg.api.pool;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.DescribeStoragePoolsResponse;
import com.gcloud.header.storage.model.StoragePoolModel;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeStoragePoolsReplyMsg extends PageReplyMessage<StoragePoolModel> {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "存储池列�?")
    private DescribeStoragePoolsResponse pools;

    @Override
    public void setList(List<StoragePoolModel> list) {
        this.pools = new DescribeStoragePoolsResponse();
        this.pools.setStoragePools(list);

    }

    public DescribeStoragePoolsResponse getPools() {
        return pools;
    }

    public void setPools(DescribeStoragePoolsResponse pools) {
        this.pools = pools;
    }

}