package com.gcloud.header.storage.msg.api.pool;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.DescribeDiskCategoriesResponse;
import com.gcloud.header.storage.model.DiskCategoryType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeDiskCategoriesReplyMsg extends PageReplyMessage<DiskCategoryType> {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "磁盘类型列表")
    private DescribeDiskCategoriesResponse diskCategories;

	@Override
	public void setList(List<DiskCategoryType> list) {
		diskCategories = new DescribeDiskCategoriesResponse();
		diskCategories.setDiskCategory(list);
	}

	public DescribeDiskCategoriesResponse getDiskCategories() {
		return diskCategories;
	}

	public void setDiskCategories(DescribeDiskCategoriesResponse diskCategories) {
		this.diskCategories = diskCategories;
	}
}