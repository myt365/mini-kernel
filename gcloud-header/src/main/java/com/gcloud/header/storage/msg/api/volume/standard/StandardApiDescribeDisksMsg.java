package com.gcloud.header.storage.msg.api.volume.standard;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class StandardApiDescribeDisksMsg extends ApiPageMessage {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "all , system , data，默认�?�为 all")
    private String diskType;
    @ApiModel(description = "磁盘状�??")
    private String status;
    @ApiModel(description = "磁盘名称")
    private String diskName;
    @ApiModel(description = "实例ID")
    private String instanceId;
    @ApiModel(description = "磁盘ID列表")
    private List<String> diskIds;
    @ApiModel(description = "挂载实例ID")
    private String attachInstanceId;

    @Override
    public Class replyClazz() {
        return StandardApiDescribeDisksReplyMsg.class;
    }

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

	public List<String> getDiskIds() {
		return diskIds;
	}

	public void setDiskIds(List<String> diskIds) {
		this.diskIds = diskIds;
	}

	public String getAttachInstanceId() {
		return attachInstanceId;
	}

	public void setAttachInstanceId(String attachInstanceId) {
		this.attachInstanceId = attachInstanceId;
	}
	
}