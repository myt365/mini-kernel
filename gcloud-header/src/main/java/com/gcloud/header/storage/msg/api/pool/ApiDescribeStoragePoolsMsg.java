package com.gcloud.header.storage.msg.api.pool;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeStoragePoolsMsg extends ApiPageMessage {

    private static final long serialVersionUID = 1L;
    
    @ApiModel(description = "存储池ID", require = false)
    private String poolId;
    
    @ApiModel(description = "存储类型, local:本地存储, distributed:分布式存�?, central:集中存储")
    private String storageType;
    
    @ApiModel(description = "可用区ID")
    private String zoneId;
    
    @ApiModel(description = "是否已经关联了的, 默认为false")
    private Boolean associated = false;
    @ApiModel(description = "关联的磁盘类型ID, 如果associated为true, 该字段为必填")
    private String associateDiskCategoryId;
    @ApiModel(description = "关联的可用区ID, 如果associated为true, 该字段为必填")
    private String associateZoneId;

    @ApiModel(description = "存储池名�?")
	private String poolName;

    @ApiModel(description = "存储池别�?")
	private String displayName;

    @ApiModel(description = "节点，模糊搜�?")
	private String hostname;

    @Override
    public Class replyClazz() {
        return ApiDescribeStoragePoolsReplyMsg.class;
    }

	public String getPoolId() {
		return poolId;
	}

	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	
	public Boolean getAssociated() {
		return associated;
	}

	public void setAssociated(Boolean associated) {
		this.associated = associated;
	}

	public String getAssociateDiskCategoryId() {
		return associateDiskCategoryId;
	}

	public void setAssociateDiskCategoryId(String associateDiskCategoryId) {
		this.associateDiskCategoryId = associateDiskCategoryId;
	}

	public String getAssociateZoneId() {
		return associateZoneId;
	}

	public void setAssociateZoneId(String associateZoneId) {
		this.associateZoneId = associateZoneId;
	}

	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}