package com.gcloud.header.storage.msg.api.volume.standard;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.standard.StandardDescribeDisksResponse;
import com.gcloud.header.storage.model.standard.StandardDiskItemType;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class StandardApiDescribeDisksReplyMsg extends PageReplyMessage<StandardDiskItemType> {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "磁盘列表")
    private StandardDescribeDisksResponse disks;

    @Override
    public void setList(List<StandardDiskItemType> list) {
        disks = new StandardDescribeDisksResponse();
        disks.setDisk(list);
    }

    public StandardDescribeDisksResponse getDisks() {
        return disks;
    }

    public void setDisks(StandardDescribeDisksResponse disks) {
        this.disks = disks;
    }
}