package com.gcloud.header.compute.msg.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;
import com.gcloud.header.network.model.IpAddressSetType;
import com.gcloud.header.network.model.NetworkInterfaces;

import java.io.Serializable;
import java.util.Date;

import org.bouncycastle.util.IPAddress;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class InstanceAttributesType implements Serializable {
    @ApiModel(description = "CPU核数")
    @TableField("core")
    private Integer cpu;
    @ApiModel(description = "内存大小，单位为 MB")
    private Integer memory;
    @ApiModel(description = "实例的显示名�?")
    @TableField("alias")
    private String instanceName;
    @ApiModel(description = "计算节点名称")
    @TableField("hostname")
    private String hostName;
    @ApiModel(description = "镜像ID")
    private String imageId;
    @ApiModel(description = "实例规格类型")
    private String instanceType;
    @ApiModel(description = "实例规格类型名称")
    private String instanceTypeName;
    @ApiModel(description = "地域ID")
    private String regionId = ControllerProperty.REGION_ID;
    @ApiModel(description = "创建时间")
    @TableField("launch_time")
    @JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
    private Date creationTime;
    @ApiModel(description = "实例状�??")
    @TableField("state")
    private String status;
    @ApiModel(description = "实例中文状�??")
    private String cnStatus;
    @ApiModel(description = "实例ID")
    @TableField("id")
    private String instanceId;
    @ApiModel(description = "可用区ID")
    private String zoneId;
    @ApiModel(description = "可用区名�?")
    private String zoneName;
    @ApiModel(description = "弹�?�公网ip集合")
    private IpAddressSetType eipAddress;
    @ApiModel(description = "内网ip集合")
    private IpAddressSetType innerIpAddress;
    @ApiModel(description = "普�?�公网ip集合")
    private IpAddressSetType publicIpAddress;
    @ApiModel(description = "实例的网卡集�?")
    private NetworkInterfaces networkInterfaces;
    @ApiModel(description = "操作系统")
    private String osType;

    private String stepState;
    private String taskState;

    public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public Integer getCpu() {
        return cpu;
    }

    public void setCpu(Integer cpu) {
        this.cpu = cpu;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(String instanceType) {
        this.instanceType = instanceType;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCnStatus() {
		return cnStatus;
	}

	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}

	public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public IpAddressSetType getEipAddress() {
        return eipAddress;
    }

    public void setEipAddress(IpAddressSetType eipAddress) {
        this.eipAddress = eipAddress;
    }

    public NetworkInterfaces getNetworkInterfaces() {
        return networkInterfaces;
    }

    public void setNetworkInterfaces(NetworkInterfaces networkInterfaces) {
        this.networkInterfaces = networkInterfaces;
    }

    public String getTaskState() {
        return taskState;
    }

    public void setTaskState(String taskState) {
        this.taskState = taskState;
    }

    public String getStepState() {
        return stepState;
    }

    public void setStepState(String stepState) {
        this.stepState = stepState;
    }

	public IpAddressSetType getInnerIpAddress() {
		return innerIpAddress;
	}

	public void setInnerIpAddress(IpAddressSetType innerIpAddress) {
		this.innerIpAddress = innerIpAddress;
	}

	public IpAddressSetType getPublicIpAddress() {
		return publicIpAddress;
	}

	public void setPublicIpAddress(IpAddressSetType publicIpAddress) {
		this.publicIpAddress = publicIpAddress;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getInstanceTypeName() {
		return instanceTypeName;
	}

	public void setInstanceTypeName(String instanceTypeName) {
		this.instanceTypeName = instanceTypeName;
	}
}