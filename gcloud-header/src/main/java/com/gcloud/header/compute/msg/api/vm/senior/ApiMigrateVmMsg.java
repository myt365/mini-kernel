package com.gcloud.header.compute.msg.api.vm.senior;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiMigrateVmMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "�?要迁移的云服务器ID", require = true)
	@NotBlank(message = "::云服务器ID不能为空")
	private String instanceId; 			// 云服务器ID
	@ApiModel(description = "�?要迁移到的节点名�?", require = true)
	@NotBlank(message = "目标节点不能为空")
    private String targetHostName;		// 目标节点
    private String poolId;      // 存储池，用于存储热迁�?
    private Integer storageType;   // 存储类型，用于存储热迁移
    private String gTaskId;
    private Boolean isTask;// 是否独立任务,防止其他任务调用公共组件，删除了任务状�??

	@Override
	public Class replyClazz() {
		return ApiMigrateVmReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getTargetHostName() {
		return targetHostName;
	}

	public void setTargetHostName(String targetHostName) {
		this.targetHostName = targetHostName;
	}

	public String getPoolId() {
		return poolId;
	}

	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}

	public Integer getStorageType() {
		return storageType;
	}

	public void setStorageType(Integer storageType) {
		this.storageType = storageType;
	}

	public String getgTaskId() {
		return gTaskId;
	}

	public void setgTaskId(String gTaskId) {
		this.gTaskId = gTaskId;
	}

	public Boolean getIsTask() {
		return isTask;
	}

	public void setIsTask(Boolean isTask) {
		this.isTask = isTask;
	}
}