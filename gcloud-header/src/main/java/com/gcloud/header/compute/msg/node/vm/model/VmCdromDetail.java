/*
 * @Date 2015-5-13
 * 
 * @Author chenyu1@g-cloud.com.cn
 * 
 * @Copyright 2015 www.g-cloud.com.cn Inc. All rights reserved.
 * 
 * 
 */
package com.gcloud.header.compute.msg.node.vm.model;

import java.io.Serializable;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class VmCdromDetail implements Serializable{

	private String isoId;
	private String isoPath;
	private String isoPoolId; 
	private String isoPool; 
	private String isoStorageType;
	private String isoTargetDevice;
	public String getIsoId() {
		return isoId;
	}
	public void setIsoId(String isoId) {
		this.isoId = isoId;
	}
	public String getIsoPath() {
		return isoPath;
	}
	public void setIsoPath(String isoPath) {
		this.isoPath = isoPath;
	}
	public String getIsoPoolId() {
		return isoPoolId;
	}
	public void setIsoPoolId(String isoPoolId) {
		this.isoPoolId = isoPoolId;
	}
	public String getIsoStorageType() {
		return isoStorageType;
	}
	public void setIsoStorageType(String isoStorageType) {
		this.isoStorageType = isoStorageType;
	}
	public String getIsoTargetDevice() {
		return isoTargetDevice;
	}
	public void setIsoTargetDevice(String isoTargetDevice) {
		this.isoTargetDevice = isoTargetDevice;
	}
	public VmCdromDetail(String isoId, String isoPath, String isoPoolId,
			String isoStorageType, String isoTargetDevice) {
		this.isoId = isoId;
		this.isoPath = isoPath;
		this.isoPoolId = isoPoolId;
		this.isoStorageType = isoStorageType;
		this.isoTargetDevice = isoTargetDevice;
	}
	public VmCdromDetail() {
	}
	public String getIsoPool() {
		return isoPool;
	}
	public void setIsoPool(String isoPool) {
		this.isoPool = isoPool;
	}
	
}