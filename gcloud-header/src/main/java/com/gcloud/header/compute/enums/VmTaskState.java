/*
 * @Date 2015-4-16
 *
 * @Author chenyu1@g-cloud.com.cn
 *
 * @Copyright 2015 www.g-cloud.com.cn Inc. All rights reserved.
 *
 * @Description 云服务器状�?�常�?
 */
package com.gcloud.header.compute.enums;

import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum VmTaskState {

	// 任务状�??
	PENDING("正在创建"),
	DELETING("正在删除"),
	LOGIC_DELETEING("正在逻辑删除"),
	ATTACH_NETCARD("正在挂载网卡"),
	DETACH_NETCARD("正在卸载网卡"),
	ATTACH_DISK("正在挂载磁盘"),
	DETACH_DISK("正在卸载磁盘"),
	ATTACH_ISO("正在挂载光盘"),
	DETACH_ISO("正在卸载光盘"),
	BUNDLING("正在打包"),
	MODIFYING_CONFIG("正在修改配置"),
	MIGRATE("正在迁移")
	;


	private String cnName;

	VmTaskState(String cnName) {
		this.cnName = cnName;
	}

	public String value() {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
	}


	public String getCnName() {
		return cnName;
	}

	public static String getCnName(String enName) {
		VmTaskState vmState = Arrays.stream(VmTaskState.values()).filter(state -> state.value().equals(enName)).findFirst().orElse(null);
		return vmState != null ? vmState.getCnName() : null;
	}

	public static Map<String, String> cnMap(){
		Map<String, String> cnMap = new HashMap<>();
		Arrays.stream(VmTaskState.values()).forEach(s -> cnMap.put(s.value(), s.getCnName()));
		return  cnMap;
	}
}