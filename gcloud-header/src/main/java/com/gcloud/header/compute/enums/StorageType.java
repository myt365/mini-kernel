package com.gcloud.header.compute.enums;

import com.gcloud.header.storage.enums.StoragePoolDriver;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum StorageType {
    LOCAL("本地存储", StoragePoolDriver.FILE),
    DISTRIBUTED("分布式存�?", StoragePoolDriver.RBD),
    CENTRAL("集中存储", StoragePoolDriver.LVM);

    private String cnName;
    private StoragePoolDriver defaultDriver;

    StorageType(String cnName, StoragePoolDriver defaultDriver) {
        this.cnName = cnName;
        this.defaultDriver = defaultDriver;
    }

    public static StorageType value(String value) {
        return Arrays.stream(StorageType.values()).filter(type -> type.getValue().equals(value)).findFirst().orElse(null);
    }

    public String getValue() {
        return name().toLowerCase();
    }

    public StoragePoolDriver getDefaultDriver() {
        return defaultDriver;
    }

    public String getCnName() {
        return cnName;
    }
}