package com.gcloud.header.image.msg.node;

import com.gcloud.header.NodeMessage;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DownloadImageMsg  extends NodeMessage {
	private String imageId;
	private String provider;//glance、gcloud
	private String imageStroageType;//file\rbd
	private String target;//节点名�?�rbd池名、vg�?
	private String targetType;//node\vg\rbd
	private String imagePath;//image 源路径\rbd池名
	private String imageResourceType;//image\iso
	private String distributeAction;//分发动作

	public String getDistributeAction() {
		return distributeAction;
	}

	public void setDistributeAction(String distributeAction) {
		this.distributeAction = distributeAction;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getImageStroageType() {
		return imageStroageType;
	}

	public void setImageStroageType(String imageStroageType) {
		this.imageStroageType = imageStroageType;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageResourceType() {
		return imageResourceType;
	}

	public void setImageResourceType(String imageResourceType) {
		this.imageResourceType = imageResourceType;
	}

}