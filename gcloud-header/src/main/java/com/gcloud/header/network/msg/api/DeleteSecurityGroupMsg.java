package com.gcloud.header.network.msg.api;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DeleteSecurityGroupMsg extends ApiMessage {
	@ApiModel(description = "安全组Id", require = true)
	@NotBlank(message = "0040401")
	private String securityGroupId;
	
	@ApiModel(description = "是否强制删除,true:�?,false:�?")
	private Boolean force;
	
	public String getSecurityGroupId() {
		return securityGroupId;
	}


	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public Boolean getForce() {
		if (force == null) {
			return Boolean.FALSE;
		} else {
			return force;
		}
	}


	public void setForce(Boolean force) {
		this.force = force;
	}


	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	
}