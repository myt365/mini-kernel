package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardVpcType implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "ID")
	private String vpcId;
	@ApiModel(description = "区域ID")
	private String regionId = ControllerProperty.REGION_ID;
	@ApiModel(description = "状�??")
	private String status;
	@ApiModel(description = "vpc名称")
	private String vpcName;
	@ApiModel(description = "中文状�??")
	private String cnStatus;
	@ApiModel(description = "子网ID列表")
	private Map<String, List> vSwitchIds;

	//TODO �?
	//cidrBlock、creationTime
	
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Map<String, List> getvSwitchIds() {
		return vSwitchIds;
	}
	public void setvSwitchIds(Map<String, List> vSwitchIds) {
		this.vSwitchIds = vSwitchIds;
	}
}