package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiNetworkInterfaceStatisticsMsg extends ApiMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "端口的拥有�??")
	private List<String> deviceOwners;
	@ApiModel(description = "是否包含没有拥有�?,true:�?,false:�?")
	private Boolean includeOwnerless;
	@ApiModel(description = "是否只显示ENI设备,true:�?,false:�?")
	private Boolean eni;

	public List<String> getDeviceOwners() {
		return deviceOwners;
	}


	public void setDeviceOwners(List<String> deviceOwners) {
		this.deviceOwners = deviceOwners;
	}


	public Boolean getIncludeOwnerless() {
		return includeOwnerless;
	}

	public void setIncludeOwnerless(Boolean includeOwnerless) {
		this.includeOwnerless = includeOwnerless;
	}

	public Boolean getEni() {
		return eni;
	}

	public void setEni(Boolean eni) {
		this.eni = eni;
	}

	@Override
	public Class replyClazz() {
		return ApiNetworkInterfaceStatisticsReplyMsg.class;
	}

}