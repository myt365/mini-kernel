package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

import javax.validation.constraints.NotBlank;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ModifyVSwitchAttributeMsg extends ApiMessage {
	@NotBlank(message = "0030201::交换机ID不能为空")
	@ApiModel(description = "交换机Id", require = true)
	private String vSwitchId;
	@ApiModel(description = "交换机名�?", require = true)
	@NotBlank(message = "0030202::交换机名称不能为�?")
	private String vSwitchName;
	@ApiModel(description = "是否启用dhcp,true:启用,false:禁用")
	private Boolean dhcp;

	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	public String getvSwitchName() {
		return vSwitchName;
	}
	public void setvSwitchName(String vSwitchName) {
		this.vSwitchName = vSwitchName;
	}
	public Boolean getDhcp() {
		return dhcp;
	}
	public void setDhcp(Boolean dhcp) {
		this.dhcp = dhcp;
	}
}