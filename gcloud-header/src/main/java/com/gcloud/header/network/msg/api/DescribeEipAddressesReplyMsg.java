package com.gcloud.header.network.msg.api;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.DescribeEipAddressesResponse;
import com.gcloud.header.network.model.EipAddressSetType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeEipAddressesReplyMsg  extends PageReplyMessage<EipAddressSetType> {
	
	@ApiModel(description = "弹�?�公网IP信息")
	private DescribeEipAddressesResponse eipAddresses;
	@Override
	public void setList(List<EipAddressSetType> list) {
		eipAddresses = new DescribeEipAddressesResponse();
		eipAddresses.setEipAddress(list);
	}
	public DescribeEipAddressesResponse getEipAddresses() {
		return eipAddresses;
	}
	public void setEipAddresses(DescribeEipAddressesResponse eipAddresses) {
		this.eipAddresses = eipAddresses;
	}
	
}