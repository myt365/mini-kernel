package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailExternalNetwork implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModel(description="网段")
	private String cidrBlock;
	@ApiModel(description="中文状�??")
	private String cnStatus;
	@ApiModel(description="外网ID")
	private String networkId;
	@ApiModel(description="网络名称")
	private String networkName;
	private String regionId = ControllerProperty.REGION_ID;
	@ApiModel(description="状�??,active:�?�?;down:失活;build:已创�?;error:错误;pending_create:创建�?;pending_update:删除�?;pending_delete:删除�?;unrecognized:未知;")
	private String status;
//	vSwitchIds: {vSwitchId: ["3912e754-1210-4f97-bebf-a79eb1e87d9d"]}
	@ApiModel(description="网络类型,VLAN、FLAT")
	private String networkType;
	@ApiModel(description="物理网络")
	private String physicalNetwork;
	@ApiModel(description="vlan�?")
	private Integer segmentId;
	@ApiModel(description="创建者ID")
	private String userId;
	@ApiModel(description="创建�?")
	private String userName;
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date creationTime;
	
	public String getCidrBlock() {
		return cidrBlock;
	}
	public void setCidrBlock(String cidrBlock) {
		this.cidrBlock = cidrBlock;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getPhysicalNetwork() {
		return physicalNetwork;
	}
	public void setPhysicalNetwork(String physicalNetwork) {
		this.physicalNetwork = physicalNetwork;
	}
	public Integer getSegmentId() {
		return segmentId;
	}
	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
}