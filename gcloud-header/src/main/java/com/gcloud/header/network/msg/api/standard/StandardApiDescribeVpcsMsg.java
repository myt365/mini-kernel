package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import javax.validation.constraints.Size;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeVpcsMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description="专有网络ID列表")
	@Size(max=20, message="0100201::列表size不能超过20")
	private List<String> vpcIds;
	
	@Override
	public Class replyClazz() {
		return StandardApiDescribeVpcsReplyMsg.class;
	}

	public List<String> getVpcIds() {
		return vpcIds;
	}

	public void setVpcIds(List<String> vpcIds) {
		this.vpcIds = vpcIds;
	}
}