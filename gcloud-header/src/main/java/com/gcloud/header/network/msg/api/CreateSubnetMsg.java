package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.AllocationPool;

import javax.validation.constraints.NotBlank;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CreateSubnetMsg extends ApiMessage {

    @Override
    public Class replyClazz() {
        return CreateSubnetReplyMsg.class;
    }

    @NotBlank(message = "0170101::子网网段不能为空")
    @ApiModel(description = "网段，形�?1.1.1.1/20", require = true)
    private String cidrBlock;
    @NotBlank(message = "0170102::网络ID不能为空")
    @ApiModel(description = "网络ID", require = true)
    private String networkId;
    @NotBlank(message = "0170103::名称不能为空")
    @ApiModel(description = "子网名称", require = true)
    private String subnetName;
    @ApiModel(description = "网关IP")
    private String gatewayIp;
    @ApiModel(description = "dns列表")
    private List<String> dnsNameServers;
    @ApiModel(description = "DHCP分配�?")
    private List<AllocationPool> allocationPools;
    @ApiModel(description = "是否启用dhcp,true:启用;false:禁用")
    private Boolean dhcp;

    public String getCidrBlock() {
        return cidrBlock;
    }

    public void setCidrBlock(String cidrBlock) {
        this.cidrBlock = cidrBlock;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getSubnetName() {
        return subnetName;
    }

    public void setSubnetName(String subnetName) {
        this.subnetName = subnetName;
    }

    public String getGatewayIp() {
        return gatewayIp;
    }

    public void setGatewayIp(String gatewayIp) {
        this.gatewayIp = gatewayIp;
    }

    public List<String> getDnsNameServers() {
        return dnsNameServers;
    }

    public void setDnsNameServers(List<String> dnsNameServers) {
        this.dnsNameServers = dnsNameServers;
    }

    public List<AllocationPool> getAllocationPools() {
        return allocationPools;
    }

    public void setAllocationPools(List<AllocationPool> allocationPools) {
        this.allocationPools = allocationPools;
    }

    public Boolean getDhcp() {
        return dhcp;
    }

    public void setDhcp(Boolean dhcp) {
        this.dhcp = dhcp;
    }
	/*@ApiModel(description = "VSwitch�?属区的ID")
	@NotBlank(message = "0030103::可用区不能为�?")
	private String zoneId;
	@ApiModel(description = "区域ID")
	private String regionId;*/

}