package com.gcloud.header.network.model.standard;

import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.SecurityGroupIdSetType;

import java.io.Serializable;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardNetworkInterfaceSet implements Serializable {
	
	@ApiModel(description = "网卡ID")
	@TableField("id")
	private String networkInterfaceId; // 弹�?�网卡的 ID
	@ApiModel(description = "网卡名称")
	@TableField("name")
	private String networkInterfaceName; // 弹�?�网卡的名称
	@ApiModel(description = "创建时间")
	@TableField("create_time")
	private String creationTime;
	@ApiModel(description = "安全组ID")
	private SecurityGroupIdSetType securityGroupIds;
	@ApiModel(description = "子网ID")
	@TableField("subnet_id")
	private String vSwitchId;
	@ApiModel(description = "网络ID")
	@TableField("router_id")
	private String vpcId;
	@ApiModel(description = "mac地址")
	private String macAddress; // 弹�?�网卡的 MAC 地址
	@ApiModel(description = "私有IP地址")
	@TableField("ip_address")
	private String privateIpAddress; // 弹�?�网卡主私有 IP 地址
	@ApiModel(description = "状�??")
	private String status; // 弹�?�网卡的状�??
	@ApiModel(description = "中文状�??")
	private String cnStatus;
	@ApiModel(description = "实例ID")
	private String instanceId; // 弹�?�网卡当前关联的实例 ID
	@ApiModel(description = "实例名称")
	private String instanceName; 
	@ApiModel(description = "实例类型")
	private String instanceType; // 弹�?�网卡当前关联的实例 ID

	public String getCnStatus() {
		return cnStatus;
	}

	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}

	public String getPrivateIpAddress() {
		return privateIpAddress;
	}

	public void setPrivateIpAddress(String privateIpAddress) {
		this.privateIpAddress = privateIpAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}

	public String getVpcId() {
		return vpcId;
	}

	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}

	public SecurityGroupIdSetType getSecurityGroupIds() {
		return securityGroupIds;
	}

	public void setSecurityGroupIds(SecurityGroupIdSetType securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
}