package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardDescribeNetworkInterfacesMsg extends ApiPageMessage {

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "VPC 的虚拟交换机 ID")
	private String vSwitchId;
	@ApiModel(description = "网卡IP")
	private String primaryIpAddress;
	@ApiModel(description = "安全组ID")
	private String securityGroupId;
	@ApiModel(description = "网卡的名�?")
	private String networkInterfaceName;
	@ApiModel(description = "实例 ID")
	private String instanceId;
	@ApiModel(description = "网卡ID列表")
	private List<String> networkInterfaceIds;
//	private String regionId;
	
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return StandardDescribeNetworkInterfacesReplyMsg.class;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}

	public String getPrimaryIpAddress() {
		return primaryIpAddress;
	}

	public void setPrimaryIpAddress(String primaryIpAddress) {
		this.primaryIpAddress = primaryIpAddress;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public List<String> getNetworkInterfaceIds() {
		return networkInterfaceIds;
	}

	public void setNetworkInterfaceIds(List<String> networkInterfaceIds) {
		this.networkInterfaceIds = networkInterfaceIds;
	}

}