package com.gcloud.header;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum Module {
	VM("虚拟机管�?", true),
	ECS("弹�?�计�?", true),
	USER("用户管理", false),
	ROLE("角色管理", false),
	API("接口管理", false),
	LOG("日志管理", true),
	SLB("负载均衡", true),
//	ENDPOINT("服务管理"),
	TENANT("租户管理", false),
	HCP("桌面�?", true),
	MONITOR("监控", true),
	REGION("区域", false)
	;

	private String cnName;
	private Boolean region;  //是否区分region


	Module(String cnName, Boolean region) {
		this.cnName = cnName;
		this.region = region;
	}

	public static Module getByValueIgnoreCase(String val){
		if(StringUtils.isBlank(val)){
			return null;
		}
		return Arrays.stream(Module.values()).filter(m -> m.value().equalsIgnoreCase(val)).findFirst().orElse(null);
	}

	public String value(){
		return name().toLowerCase();
	}

	public String getCnName() {
		return cnName;
	}

	public Boolean getRegion() {
		return region;
	}
}