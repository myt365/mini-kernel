package com.gcloud.header.log.msg.api;

import java.util.Date;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeLogsMsg extends ApiPageMessage{
	@ApiModel(description="用户�?")
	private String userName;
	@ApiModel(description="操作功能")
	private String funName;
	@ApiModel(description="操作�?始时�?")
	private String startTime;
	@ApiModel(description="操作结束时间")
	private String endTime;
	@ApiModel(description="是否只是长任�?")
	private boolean onlyLongTask;
	@ApiModel(description = "状�??")
	private Byte result;

	@Override
	public Class replyClazz() {
		return ApiDescribeLogsReplyMsg.class;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFunName() {
		return funName;
	}

	public void setFunName(String funName) {
		this.funName = funName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public boolean isOnlyLongTask() {
		return onlyLongTask;
	}

	public void setOnlyLongTask(boolean onlyLongTask) {
		this.onlyLongTask = onlyLongTask;
	}

	public Byte getResult() {
		return result;
	}

	public void setResult(Byte result) {
		this.result = result;
	}
}