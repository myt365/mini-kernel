package com.gcloud.header.identity.ldap;

import com.gcloud.header.api.ApiModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LdapOrgNode implements Serializable{
	@ApiModel(description ="域组织名")
	private String name;
	@ApiModel(description ="域DN")
	private String dn;
//	@ApiModel(description ="域组织叶子节�?")
	private List<LdapOrgNode> children = new ArrayList<LdapOrgNode>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	public List<LdapOrgNode> getChildren() {
		return children;
	}
	public void setChildren(List<LdapOrgNode> children) {
		this.children = children;
	}
	
}