package com.gcloud.header.identity.ldap;

import java.io.Serializable;
import java.util.Date;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LdapLogItem implements Serializable{
	@ApiModel(description = "域id")
	private Integer id;
	@ApiModel(description = "域名")
	private String domain;//域名
	@ApiModel(description = "域服务器IP")
	private String domainIp;//域服务器IP
	@ApiModel(description = "部门")
	private String department;//部门
	@ApiModel(description = "用户�?")
	private String username;//用户�?
	@ApiModel(description = "操作类型,add:新增,delete:删除,update:更新")
	private String actType;//操作类型
	@ApiModel(description = "创建时间")
	private Date createTime;//创建时间
	@ApiModel(description = "操作结果,success:成功,fail:失败")
	private String result;//操作结果
	public Integer getId() {
		return id;
	}
	public String getDomain() {
		return domain;
	}
	public String getDomainIp() {
		return domainIp;
	}
	public String getDepartment() {
		return department;
	}
	public String getUsername() {
		return username;
	}
	public String getActType() {
		return actType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public String getResult() {
		return result;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public void setDomainIp(String domainIp) {
		this.domainIp = domainIp;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setActType(String actType) {
		this.actType = actType;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public void setResult(String result) {
		this.result = result;
	}
}