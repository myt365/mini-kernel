package com.gcloud.header.identity.ldap;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiTestLdapConnectMsg extends ApiMessage{
	@ApiModel(description = "URL",require = true)
	@Length(min = 7, max = 100, message = "identity_server_ldap_300001")
	private String url;
	@ApiModel(description = "帐号",require = true)
	@Length(min=4,max=20,message="identity_server_ldap_300002")
	private String managerUser;
	@ApiModel(description = "密码",require = true)
	@Length(min=6,max=20,message="identity_server_ldap_300003")
	private String managerPsw;
	private String id;
	@NotBlank(message="identity_server_ldap_300005")
	@ApiModel(description = "域名",require = true)
	private String domain;
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return ApiTestLdapConnectReplyMsg.class;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getManagerUser() {
		return managerUser;
	}
	public void setManagerUser(String managerUser) {
		this.managerUser = managerUser;
	}
	public String getManagerPsw() {
		return managerPsw;
	}
	public void setManagerPsw(String managerPsw) {
		this.managerPsw = managerPsw;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	
}