package com.gcloud.header.security.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class SecurityClusterDetailResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "安全集群ID")
	private String id;
	@ApiModel(description = "安全集群名称")
    private String name;
	@ApiModel(description = "")
    private String protectionNetCidr;
	@ApiModel(description = "安全集群状�??")
    private String state;
	@ApiModel(description = "安全集群中文状�??")
    private String stateCnName;
	@ApiModel(description = "安全集群创建�?")
    private String createUser;
	@ApiModel(description = "安全集群创建者名�?")
    private String createUserName;
	@ApiModel(description = "创建时间")
    private String createTime;
	@ApiModel(description = "安全集群信息")
	private SecurityClusterInfoType clusterInfo;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProtectionNetCidr() {
		return protectionNetCidr;
	}
	public void setProtectionNetCidr(String protectionNetCidr) {
		this.protectionNetCidr = protectionNetCidr;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateCnName() {
		return stateCnName;
	}
	public void setStateCnName(String stateCnName) {
		this.stateCnName = stateCnName;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public SecurityClusterInfoType getClusterInfo() {
		return clusterInfo;
	}
	public void setClusterInfo(SecurityClusterInfoType clusterInfo) {
		this.clusterInfo = clusterInfo;
	}
}