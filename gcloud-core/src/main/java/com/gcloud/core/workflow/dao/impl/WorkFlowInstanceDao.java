package com.gcloud.core.workflow.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.gcloud.core.workflow.dao.IWorkFlowInstanceDao;
import com.gcloud.core.workflow.entity.WorkFlowInstance;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Repository
public class WorkFlowInstanceDao extends JdbcBaseDaoImpl<WorkFlowInstance, Long> implements IWorkFlowInstanceDao{

	@Override
	public List<WorkFlowInstance> getFlowsByIds(List<Long> ids) {
		if (ids.size() > 0) {
			StringBuffer sql = new StringBuffer();
			sql.append("select * from gc_work_flow_instance w where ");
			
			sql.append(" w.id in (");
			sql.append(ids.stream().map(id -> id.toString()).collect(Collectors.joining(",")));
			sql.append(") ");
			
			return this.findBySql(sql.toString());
		}
		return new ArrayList<WorkFlowInstance>();
	}

	@Override
	public void deleteExpireData(String endTime) {
		String sql ="delete from gc_work_flow_instance where start_time <= '" + endTime + "'";
		this.jdbcTemplate.execute(sql);
	}

	@Override
	public Long getMaxInstanceId(String endTime) {
		String sql ="select MAX(id) as id from gc_work_flow_instance where start_time <= '" + endTime + "'";
		List<Map<String, Object>> results = this.jdbcTemplate.queryForList(sql);
		return results.get(0).get("id")==null?null:Long.parseLong(results.get(0).get("id").toString());
	}

}