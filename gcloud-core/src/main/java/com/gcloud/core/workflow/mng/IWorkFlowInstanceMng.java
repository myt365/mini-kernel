package com.gcloud.core.workflow.mng;

import java.util.List;

import com.gcloud.core.workflow.entity.WorkFlowInstance;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IWorkFlowInstanceMng {
	void update(WorkFlowInstance ins, List<String> fields);
	Long save(WorkFlowInstance ins);
	WorkFlowInstance findById(Long id);
	WorkFlowInstance findUnique(String field,String value);
	WorkFlowInstance getSubFlow(Long flowId, int stepId);
}