package com.gcloud.core.workflow.mng;

import java.util.Date;
import java.util.List;

import com.gcloud.core.workflow.entity.FlowTask;
import com.gcloud.core.workflow.entity.WorkFlowInstance;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IFlowTaskMng {
	void update(FlowTask task, List<String> fields);
	Long save(FlowTask task);
	FlowTask findById(Long id);
	FlowTask findUnique(String field,String value);
	List<WorkFlowInstance> getSubFlows(Long flowId, int stepId);
	void deleteWorkflowExpireData(String endTime);
}