package com.gcloud.core.cache.redis.template;

import com.gcloud.core.util.SerializeUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class GCloudRedisTemplate extends StringRedisTemplate {

    public Object getObject(String key){
        String str = opsForValue().get(key);
        return SerializeUtils.unSerialize(str);
    }

    public void setObject(String key, Object object){
        opsForValue().set(key, SerializeUtils.serialize(object));
    }

}