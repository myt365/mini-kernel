package com.gcloud.core.cache;

import java.util.HashMap;
import java.util.Map;

import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.service.SpringUtil;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public abstract class Cache<T> {
	private boolean hasInit=false;
	private static Map<CacheType,Cache> caches=new HashMap<CacheType, Cache>();
	public static void register(Class c){
		Cache cache=null;
		try {
			cache = (Cache) SpringUtil.getBean(c);
			caches.put(cache.getType(), cache);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static Map<CacheType,Cache> getMember(){
		return caches;
	}
	
	public abstract Map<String,T> requestCache();
	public abstract CacheType getType();
	public abstract T getValue(String key);
	
	public void init(){
		Map<String,T> map=null;
		try{
			map=requestCache();
		}catch(Exception e){
			e.printStackTrace();
		}
		if(map!=null){
			CacheContainer container = CacheContainer.getInstance();
			container.put(getType(), map);
			hasInit=true;
		}
	}
	
	/**
	 * 是否定时重新初始�?
	 * @return
	 */
	public boolean reInit(){
		return true;
	}
	/**
	 * 初始化的时间隔，单秒
	 * @return
	 */
	public int interval(){
		return 60;
	}

	public boolean isHasInit() {
		return hasInit;
	}

	public void setHasInit(boolean hasInit) {
		this.hasInit = hasInit;
	}
}