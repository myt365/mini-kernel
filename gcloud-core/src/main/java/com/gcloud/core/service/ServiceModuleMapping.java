package com.gcloud.core.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.header.Module;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
public class ServiceModuleMapping {
	@Autowired
	private ServiceName serverName;
	
	private Map<String,String> uriServiceMap=new HashMap<>();
	
	@PostConstruct
	public void init() {
		uriServiceMap.put(Module.USER.name(), serverName.getIdentity());
		uriServiceMap.put(Module.TENANT.name(), serverName.getIdentity());
	}
	public String getService(String module) {
		String service=uriServiceMap.get(module);
		if(service==null)
			return serverName.getController();
		return service;
	}
}