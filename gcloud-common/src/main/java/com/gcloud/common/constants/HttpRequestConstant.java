package com.gcloud.common.constants;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class HttpRequestConstant {
	public static final String HEADER_TOKEN_ID = "X-Auth-Token";
    public static final String HEADER_USER_ID = "X-Auth-User-Id";
    public static final String HEADER_TENANT_ID = "TenantId";

    public static final String ATTR_LOGIN_USER = "X-Login-User";
    public static final String INVALIDATE_ERRORCODE = "identity_token_auth_0001::token is not validate"; //统一没有权限的错误码
    public static final String ATTR_TASK_ID = "X-Task-Id";
    public static final String ATTR_REQUEST_ID = "x-request-id";
    public static final String ATTR_USER_INFO = "x-user-info";
    public static final String ATTR_RESOURCE_RIGHT = "x-resource-right";

    public static final String SESSION_USER_INFO = "user-info";
    
}