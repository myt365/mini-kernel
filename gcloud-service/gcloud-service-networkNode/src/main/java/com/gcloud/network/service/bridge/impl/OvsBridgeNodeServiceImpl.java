package com.gcloud.network.service.bridge.impl;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.network.service.bridge.IOvsBridgeNodeService;
import com.gcloud.service.common.util.NetworkNodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Component
public class OvsBridgeNodeServiceImpl implements IOvsBridgeNodeService {

    @Override
    public void createOvsBridge(String bridgeName) {

        try{
            if(!NetworkNodeUtil.isOvsBrExist(bridgeName)){
                NetworkNodeUtil.addOvsBr(bridgeName);
            }
        }catch (Exception ex){
            if(!NetworkNodeUtil.isOvsBrExist(bridgeName)){
                log.error("1010604::创建ovs网桥失败", ex);
                throw new GCloudException("1010604::创建ovs网桥失败");
            }
        }

    }

    @Override
    public void deleteOvsBridge(String bridgeName) {

        try{
            if(NetworkNodeUtil.isOvsBrExist(bridgeName)){
                NetworkNodeUtil.deleteOvsBr(bridgeName);
            }
        }catch (Exception ex){
            if(NetworkNodeUtil.isOvsBrExist(bridgeName)){
                log.error("::删除ovs网桥失败", ex);
                throw new GCloudException("::删除ovs网桥失败");
            }
        }

    }
}