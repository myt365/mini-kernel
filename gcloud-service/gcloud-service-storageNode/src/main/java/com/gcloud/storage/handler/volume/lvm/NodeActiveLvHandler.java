package com.gcloud.storage.handler.volume.lvm;

import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.storage.msg.node.volume.lvm.NodeActiveLvMsg;
import com.gcloud.header.storage.msg.node.volume.lvm.NodeActiveLvReplyMsg;
import com.gcloud.service.common.compute.model.QemuInfo;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import com.gcloud.service.common.lvm.uitls.LvmUtil;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class NodeActiveLvHandler extends MessageHandler<NodeActiveLvMsg, NodeActiveLvReplyMsg>{

	@Override
	public NodeActiveLvReplyMsg handle(NodeActiveLvMsg msg) throws GCloudException {
		NodeActiveLvReplyMsg reply = new NodeActiveLvReplyMsg();
		reply.setSuccess(true);
		try {
			QemuInfo info = DiskQemuImgUtil.info(msg.getLvPath());
			if(StringUtils.isNotBlank(info.getBackingFile())) {
				LvmUtil.activeLv(info.getBackingFile());
			}
			LvmUtil.activeLv(msg.getLvPath());
		}catch(GCloudException gex) {
			reply.setErrorMsg(gex.getMessage());
			reply.setSuccess(false);
		}
        
        return reply;
	}

}