/*
 * @Date 2019�?2�?12�?
 * 
 * @Author kongmq@g-cloud.com.cn
 * 
 * @Copyright 2019 www.g-cloud.com.cn Inc. All rights reserved.
 * 
 * @Description
 */

package com.gcloud.api.filter;

import com.gcloud.common.util.HttpUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.core.util.SslUtils;
import com.gcloud.header.GcloudConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class MonitorRouterFilter implements Filter {

	@Value("${gcloud.api.monitor.url:http://127.0.0.1}")
	private String mointorUrl;

	@Value("${gcloud.api.monitor.token:}")
	private String token;

	@Value("${gcloud.api.monitor.regionId:}")
	private String regionId;

	@Autowired
	private RequestRouter router;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String targetUrl = HttpUtil.url(mointorUrl, httpRequest.getRequestURI());
		if (httpRequest.getRequestURI().startsWith("/monitor/")) {
			ModifiableHttpServletRequest modifyReq = new ModifiableHttpServletRequest(httpRequest);
			if (StringUtils.isNotBlank(token)) {
				modifyReq.putHeader("Authorization", token);
			}
			if (mointorUrl.startsWith("https")) {
				try {
					SslUtils.ignoreSsl();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			modifyReq.addParameter("regionId", regionId);
			modifyReq.addParameter("platformType", GcloudConstants.MONITOR_GCLOUD8_PLATFORM_TYPE);
			modifyReq.addParameter("manageType", "gcloud8");
			router.route(targetUrl, modifyReq, httpResponse);
			return;
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}

}