package com.gcloud.api.handler;

import com.gcloud.api.ApiRole;
import com.gcloud.api.condition.ApiRoleSelect;
import com.gcloud.api.service.RegionService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.region.msg.api.ApiDeleteRegionMsg;
import com.gcloud.header.region.msg.api.ApiDeleteRegionReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.REGION, action = "DeleteRegion", name = "删除区域")
@ApiRoleSelect(ApiRole.API)
@GcLog(taskExpect = "删除区域")
public class ApiDeleteRegionsHandler extends MessageHandler<ApiDeleteRegionMsg, ApiDeleteRegionReplyMsg>{

	@Autowired
	private RegionService regionService;

	@Override
	public ApiDeleteRegionReplyMsg handle(ApiDeleteRegionMsg msg) throws GCloudException {
		regionService.delete(msg.getId());
		return new ApiDeleteRegionReplyMsg();
	}
}