package com.gcloud.controller.image.prop;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@ConfigurationProperties(prefix = "gcloud.controller.image")
public class ImageProp {
	 @Value("${gcloud.controller.image.stroageType:file}")
	 private String stroageType;//镜像后端存储类型 file\lvm\rbd
	 
	@Value("${gcloud.controller.image.filesystemStoreDir:/var/lib/images/}")
	private String imageFilesystemStoreDir;
	
	@Value("${gcloud.controller.image.iso.filesystemStoreDir:/var/lib/isos/}")
	private String isoFilesystemStoreDir;

	public String getStroageType() {
		return stroageType;
	}

	public void setStroageType(String stroageType) {
		this.stroageType = stroageType;
	}

	public String getImageFilesystemStoreDir() {
		return imageFilesystemStoreDir;
	}

	public void setImageFilesystemStoreDir(String imageFilesystemStoreDir) {
		this.imageFilesystemStoreDir = imageFilesystemStoreDir;
	}

	public String getIsoFilesystemStoreDir() {
		return isoFilesystemStoreDir;
	}

	public void setIsoFilesystemStoreDir(String isoFilesystemStoreDir) {
		this.isoFilesystemStoreDir = isoFilesystemStoreDir;
	}
	
}