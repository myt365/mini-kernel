package com.gcloud.controller.image.service;

import java.util.List;
import java.util.Map;

import com.gcloud.controller.image.entity.ImageStore;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IImageStoreService {
	void update(ImageStore store, List<String> fields);
	void delete(ImageStore store);
	ImageStore findUniqueByProperties(Map<String, Object> props);
	ImageStore getById(String id);
}