package com.gcloud.controller.image.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.model.CreateImageParams;
import com.gcloud.controller.image.model.DistributeImageParams;
import com.gcloud.controller.image.model.UploadImageParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.image.msg.api.GenDownloadVo;
import org.springframework.core.io.Resource;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IImageProvider extends IResourceProvider {

    String createImage(CreateImageParams params, CurrentUser currentUser) throws GCloudException;

    void updateImage(String imageId, String imageProviderRefId, String imageName) throws GCloudException;

    void deleteImage(String imageId, String imageProviderRefId) throws GCloudException;

    List<Image> listImage(Map<String, String> filters) throws GCloudException;

    GenDownloadVo genDownload(String imageId, String providerRefId);

    Resource download(String imageId);

    void distributeImage(DistributeImageParams params);

    String uploadImage(UploadImageParams params, CurrentUser currentUser) throws GCloudException;
}