package com.gcloud.controller.image.handler.api.iso;

import com.gcloud.controller.image.workflow.iso.DeleteGcloudIsoWorkflow;
import com.gcloud.controller.image.workflow.model.iso.DeleteGcloudIsoInitFlowCommandRes;
import com.gcloud.controller.image.workflow.model.iso.DeleteGcloudIsoWorkflowReq;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.msg.api.iso.ApiDeleteIsoMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@LongTask
@GcLog(isMultiLog = true, taskExpect = "删除映像")
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DeleteIso",name="删除映像")
public class ApiDeleteIsoHandler extends BaseWorkFlowHandler<ApiDeleteIsoMsg, ApiReplyMessage> {
    
    @Override
    public Object initParams(ApiDeleteIsoMsg msg) {
    	DeleteGcloudIsoWorkflowReq req = new DeleteGcloudIsoWorkflowReq();
    	req.setIsoId(msg.getIsoId());
    	return req;
    }

	@Override
	public Object preProcess(ApiDeleteIsoMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public ApiReplyMessage process(ApiDeleteIsoMsg msg) throws GCloudException {
		ApiReplyMessage replyMessage = new ApiReplyMessage();
		DeleteGcloudIsoInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DeleteGcloudIsoInitFlowCommandRes.class);
		replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getIsoId())
				.objectName(CacheContainer.getInstance().getString(CacheType.ISO_NAME, msg.getIsoId())).expect("删除映像").build());

        return replyMessage;
	}

	@Override
	public Class getWorkflowClass() {
		return DeleteGcloudIsoWorkflow.class;
	}
}