package com.gcloud.controller.image.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.service.IImageService;
import com.gcloud.controller.image.workflow.model.UnactiveLvImageInitFlowCommandReq;
import com.gcloud.controller.image.workflow.model.UnactiveLvImageInitFlowCommandRes;
import com.gcloud.controller.storage.service.IStorageLvNodeService;
import com.gcloud.controller.storage.workflow.model.volume.UnactiveLvDiskInitFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class UnactiveLvImageInitFlowCommand extends BaseWorkFlowCommand {
	
	@Autowired
	IImageService imgService;
	
	@Autowired
    private IStorageLvNodeService storageLvNodeService;

	@Override
	protected Object process() throws Exception {
		UnactiveLvImageInitFlowCommandReq req = (UnactiveLvImageInitFlowCommandReq)getReqParams();
		/*Image img = imgService.getById(req.getImageId());
		if(img == null) {
			throw new GCloudException(":: 该镜像不存在");
		}*/
		UnactiveLvDiskInitFlowCommandRes res = new UnactiveLvDiskInitFlowCommandRes();
		res.setLvNodes(storageLvNodeService.findByImageId(req.getImageId()));
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return UnactiveLvImageInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return UnactiveLvImageInitFlowCommandRes.class;
	}

}