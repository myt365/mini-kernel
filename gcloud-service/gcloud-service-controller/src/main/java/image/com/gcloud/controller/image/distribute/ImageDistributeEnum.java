package com.gcloud.controller.image.distribute;

import com.gcloud.core.service.SpringUtil;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum ImageDistributeEnum {
	FILE_TO_NODE("file", "node", FileToNodeDistributeImageImpl.class),
	FILE_TO_VG("file", "vg", FileToVgDistributeImageImpl.class),
	FILE_TO_RBD("file", "rbd", FileToRbdDistributeImageImpl.class);

	private String storageType;
	private String target;
	private Class clazz;
	
	ImageDistributeEnum(String storageType, String target, Class clazz){
		this.storageType = storageType;
		this.target = target;
		this.clazz = clazz;
	}
	
	public static IDistributeImage getByType(String type, String target) {
		for (ImageDistributeEnum driverE : ImageDistributeEnum.values()) {
			if(driverE.getStorageType().equals(type.toLowerCase()) && driverE.getTarget().equals(target)) {
				return (IDistributeImage)SpringUtil.getBean(driverE.clazz);
			}
		}
		return null;
	}
	
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Class getClazz() {
		return clazz;
	}
	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}
}