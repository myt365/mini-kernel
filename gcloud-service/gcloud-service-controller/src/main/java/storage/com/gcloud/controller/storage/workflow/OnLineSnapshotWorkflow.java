package com.gcloud.controller.storage.workflow;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.workflow.model.volume.OnLineSnapshotWorkflowPreRes;
import com.gcloud.controller.storage.workflow.model.volume.OnLineSnapshotWorkflowReq;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import com.gcloud.header.compute.enums.PlatformType;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.compute.msg.node.vm.base.CheckInstanceDomainStateMsg;
import com.gcloud.header.compute.msg.node.vm.base.CheckInstanceDomainStateReplyMsg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class OnLineSnapshotWorkflow extends BaseWorkFlows{
	@Autowired
    private VolumeAttachmentDao volumeAttachmentDao;
    
    @Autowired
    private InstanceDao instanceDao;
    
    @Autowired
    private MessageBus bus;
	
	@Override
	public String getFlowTypeCode() {
		return "OnLineSnapshotWorkflow";
	}

	@Override
	public Object preProcess() {
		OnLineSnapshotWorkflowReq req = (OnLineSnapshotWorkflowReq)getReqParams();
		OnLineSnapshotWorkflowPreRes res = new OnLineSnapshotWorkflowPreRes();
		List<VolumeAttachment> attachs = volumeAttachmentDao.findByProperty("volumeId", req.getDiskId());
		boolean needFsFreeze = false;
        if(attachs.size() > 0) {
        	VmInstance vm = instanceDao.getById(attachs.get(0).getInstanceUuid());
        	res.setInstanceId(attachs.get(0).getInstanceUuid());
        	
        	CheckInstanceDomainStateMsg msg = new CheckInstanceDomainStateMsg();
			msg.setServiceId(MessageUtil.computeServiceId(vm.getHostname()));
			msg.setInstanceId(vm.getId());

			CheckInstanceDomainStateReplyMsg reply = bus.call(msg, CheckInstanceDomainStateReplyMsg.class);
        	
        	if(!reply.getState().equals(VmState.STOPPED.value())) {
//        		needFsFreeze = true;
    			throw new GCloudException("::é?è¦å¨å³æºæè?å¸è½½çç¶æ?ä¸æè½è¿è¡å¿«ç§æä½");
    		}
        	/*if(vm.getPlatform().equals(PlatformType.WINDOWS.getValue())) {
        		//windows é?è¦å³æºç¶æ?
        		if(!reply.getState().equals(VmState.STOPPED.value())) {
        			throw new GCloudException("::windowsèææºéè¦å¨å³æºçç¶æä¸æè½è¿è¡å¿«ç§æä½");
        		}
        	}*/
        	
        }
        
        res.setNeedFsFreeze(needFsFreeze);
        
		return res;
	}

	@Override
	public void process() {
		
	}

	@Override
	protected Class<?> getReqParamClass() {
		return OnLineSnapshotWorkflowReq.class;
	}

}