package com.gcloud.controller.storage.handler.api.volume.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.volume.standard.StandardApiDeleteDiskMsg;
import com.gcloud.header.storage.msg.api.volume.standard.StandardApiDeleteDiskReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@LongTask
@GcLog(taskExpect = "删除磁盘")
@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="DeleteDisk", versions = {ApiVersion.Standard}, name = "删除磁盘")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.VOLUME, resourceIdField = "diskId")
public class StandardApiDeleteDiskHandler extends MessageHandler<StandardApiDeleteDiskMsg, StandardApiDeleteDiskReplyMsg>{

	@Autowired
    private IVolumeService volumeService;
	
	@Override
	public StandardApiDeleteDiskReplyMsg handle(StandardApiDeleteDiskMsg msg) throws GCloudException {
		volumeService.deleteVolume(msg.getDiskId(), msg.getTaskId());
        msg.setObjectId(msg.getDiskId());
        msg.setObjectName(CacheContainer.getInstance().getString(CacheType.VOLUME_NAME, msg.getDiskId()));
        return new StandardApiDeleteDiskReplyMsg();
	}

}