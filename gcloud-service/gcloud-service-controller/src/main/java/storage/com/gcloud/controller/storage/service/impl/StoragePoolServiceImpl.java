package com.gcloud.controller.storage.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.compute.dao.ComputeNodeDao;
import com.gcloud.controller.compute.dao.ZoneDao;
import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.controller.compute.entity.ComputeNode;
import com.gcloud.controller.storage.dao.DiskCategoryDao;
import com.gcloud.controller.storage.dao.DiskCategoryPoolDao;
import com.gcloud.controller.storage.dao.DiskCategoryZoneDao;
import com.gcloud.controller.storage.dao.StoragePoolCollectCronTaskDao;
import com.gcloud.controller.storage.dao.StoragePoolCollectDetailDao;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.dao.StoragePoolNodeDao;
import com.gcloud.controller.storage.dao.StoragePoolZoneDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.controller.storage.entity.DiskCategoryPool;
import com.gcloud.controller.storage.entity.DiskCategoryZone;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.StoragePoolCollectCronTask;
import com.gcloud.controller.storage.entity.StoragePoolCollectDetail;
import com.gcloud.controller.storage.entity.StoragePoolNode;
import com.gcloud.controller.storage.entity.StoragePoolZone;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.enums.CollectStorageInfoState;
import com.gcloud.controller.storage.model.AssociateDiskCategoryMorePoolParams;
import com.gcloud.controller.storage.model.AssociateDiskCategoryPoolParams;
import com.gcloud.controller.storage.model.AssociateDiskCategoryZoneParams;
import com.gcloud.controller.storage.model.AssociatePoolZoneParams;
import com.gcloud.controller.storage.model.CreateDiskCategoryParams;
import com.gcloud.controller.storage.model.DeleteDiskCategoryParams;
import com.gcloud.controller.storage.model.DescribeDiskCategoriesParams;
import com.gcloud.controller.storage.model.DescribeStoragePoolsParams;
import com.gcloud.controller.storage.model.DetailDiskCategoryParams;
import com.gcloud.controller.storage.model.DetailPoolParams;
import com.gcloud.controller.storage.model.DiskCategoryInfo;
import com.gcloud.controller.storage.model.EnableDiskCategoryParams;
import com.gcloud.controller.storage.model.ModifyDiskCategoryParams;
import com.gcloud.controller.storage.model.UnassociateDiskCategoryPoolParams;
import com.gcloud.controller.storage.model.node.ReportStoragePoolParams;
import com.gcloud.controller.storage.provider.IStoragePoolProvider;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.controller.storage.util.StorageControllerUtil;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.cache.redis.lock.util.LockUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.StorageDiskProtocol;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import com.gcloud.header.storage.model.DiskCategoryModel;
import com.gcloud.header.storage.model.DiskCategoryType;
import com.gcloud.header.storage.model.DiskCategoryZonePool;
import com.gcloud.header.storage.model.StoragePoolDetailInfo;
import com.gcloud.header.storage.model.StoragePoolInfo;
import com.gcloud.header.storage.model.StoragePoolModel;
import com.gcloud.header.storage.model.StoragePoolType;
import com.gcloud.header.storage.model.StoragePoolZoneInfo;
import com.gcloud.header.storage.model.StorageTypeVo;
import com.gcloud.header.storage.msg.api.pool.ApiPoolsStatisticsReplyMsg;
import com.gcloud.header.storage.msg.node.pool.LocalPoolReportMsg;
import com.gcloud.header.storage.msg.node.pool.model.LocalPoolReport;
import com.gcloud.service.common.Consts;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
@Transactional(propagation = Propagation.REQUIRED)
@Component
public class StoragePoolServiceImpl implements IStoragePoolService {

    @Autowired
    private StoragePoolDao poolDao;

    @Autowired
    private DiskCategoryDao categoryDao;
    
    @Autowired
    private DiskCategoryZoneDao diskCategoryZoneDao;
    
    @Autowired
    private DiskCategoryPoolDao diskCategoryPoolDao;

    @Autowired
    private ComputeNodeDao nodeDao;
    
    @Autowired
    private VolumeDao volumeDao;
    
    @Autowired
    private ZoneDao zoneDao;
    
    @Autowired
    private StoragePoolZoneDao storagePoolZoneDao;
    
    @Autowired
    private StoragePoolNodeDao storagePoolNodeDao;
    
    @Autowired
    private StoragePoolCollectCronTaskDao storagePoolCollectCronTaskDao;
    
    @Autowired
    private StoragePoolCollectDetailDao storagePoolCollectDetailDao;

    @Autowired
	private ComputeNodeDao computeNodeDao;

    @Override
    public List<DiskCategoryModel> describeDiskCategories(String zoneId) {
        return categoryDao.diskCategoryList(zoneId, DiskCategoryModel.class);
    }
    
    
    @Override
   	public PageResult<DiskCategoryType> describeDiskCategories(DescribeDiskCategoriesParams params) {

    	if(StringUtils.isNotBlank(params.getAvailableForHost())){
			ComputeNode node = computeNodeDao.findUniqueByProperty(ComputeNode.HOSTNAME, params.getAvailableForHost());
			if(node == null){
				throw new GCloudException("::节点不存�?");
			}
			params.setAvailableForHostZone(params.getZoneId());
		}

    	return categoryDao.describeDiskCategories(params, DiskCategoryType.class);
   	}

	@Override
    public String createDiskCategory(CreateDiskCategoryParams params) {
		String strStorageType = params.getStorageType();
		StorageType storageType = StorageType.value(strStorageType);
		if(null == storageType) {
			log.error("::存储类型无法识别");
			throw new GCloudException("::存储类型无法识别");
		}
		
    	//磁盘类型数据
        DiskCategory entity = new DiskCategory();
        entity.setId(StringUtils.genUuid());
        entity.setName(params.getName());
        entity.setEnabled(params.isEnabled());
        entity.setCreateTime(new Date());
        
        Integer min = params.getMinSize() == null ? 0 : params.getMinSize();
        Integer max = params.getMaxSize() == null ? 0 : params.getMaxSize();
        
        if(min > max) {
        	log.error("::磁盘类型的最小�?�不能大于磁盘类型的�?大�??");
			throw new GCloudException("::磁盘类型的最小�?�不能大于磁盘类型的�?大�??");
        }
        
        entity.setMinSize(min);
        entity.setMaxSize(max);
        
        entity.setStorageType(storageType.getValue());
        
        try{
        	this.categoryDao.save(entity);
        } catch(Exception e) {
        	log.error("创建磁盘类型失败，原因：�?"+ e.getCause() + "::" + e.getMessage() +"�?");
        	throw new GCloudException("::创建磁盘类型失败");
        }
        return entity.getId();
    }

    @Override
    public PageResult<StoragePoolModel> describeStoragePools(int pageNumber, int pageSize, String poolId) {
        return this.poolDao.describeStoragePools(pageNumber, pageSize, poolId, StoragePoolModel.class);
    }
    
    @Override
	public PageResult<StoragePoolModel> describeStoragePools(DescribeStoragePoolsParams params) {
    	if(params.getAssociated() != null && params.getAssociated().booleanValue()) {
    		if(StringUtils.isBlank(params.getAssociateDiskCategoryId())) {
    			log.error("::查询没有关联的存储池，磁盘类型ID不能为空");
    			throw new GCloudException("::查询没有关联的存储池，磁盘类型ID不能为空");
    		}
    		if(StringUtils.isBlank(params.getAssociateZoneId())) {
    			log.error("::查询没有关联的存储池，可用区ID不能为空");
    			throw new GCloudException("::查询没有关联的存储池，可用区ID不能为空");
    		}
    	}
    	return this.poolDao.describeStoragePools(params, StoragePoolModel.class);
	}

    private DiskCategory checkAndGetCategory(String categoryId) throws GCloudException {
        if (categoryId != null) {
            DiskCategory res = this.categoryDao.getById(categoryId);
            if (res != null) {
                return res;
            }
        }
        throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
    }

    private DiskCategory checkAndGetCategory(String zoneId, String code) throws GCloudException {
        Map<String, Object> props = new HashMap<>();
        props.put("zoneId", zoneId);
        props.put("code", code);
        List<DiskCategory> res = this.categoryDao.findByProperties(props);
        if (res.isEmpty()) {
            throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
        }
        return res.get(0);
    }

    @Override
    public String createStoragePool(String displayName, Integer providerType, String storageType, String poolName, String zoneId, /*String categoryId,*/ String hostname,
                                    String driverName, String taskId) throws GCloudException {
        StorageType type = StorageType.value(storageType);
        if (type == null) {
            throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
        }
        AvailableZoneEntity zone = zoneDao.getById(zoneId);
		if(null == zone) {
			log.error("::不存在该可用�?");
			throw new GCloudException("::不存在该可用�?");
		}
        StoragePoolDriver driver = this.checkAndGetDriver(type, driverName);
        //TODO 新建存储池不�?要磁盘类�?
        //this.checkAndGetCategory(categoryId);
        Integer provider = providerType;
        if(providerType == null) {
        	provider = ResourceProviders.getDefault(ResourceType.STORAGE_POOL).providerType().getValue();
        }

        //StorageDiskProtocol protocol = StorageDiskProtocol.getByProviderAndStorageType(providerType, driver.name());
        StorageDiskProtocol protocol = StorageDiskProtocol.getByProviderAndStorageType(provider, driver.name());
        if (protocol == null) {
            throw new GCloudException(StorageErrorCodes.CONNECT_PROTOCOL_NOT_EXISTS);
        }

        StoragePool pool = new StoragePool();
        pool.setId(StringUtils.genUuid());

//        if (this.poolDao.find(providerType, storageType, poolName, hostname) != null) {
//            throw new GCloudException(StorageErrorCodes.POOL_ALREADY_EXISTS);
//        }
        
        if (this.poolDao.find(provider, storageType, poolName, hostname) != null) {
            throw new GCloudException(StorageErrorCodes.POOL_ALREADY_EXISTS);
        }

        //String refId = this.getProvider(providerType).createStoragePool(pool.getId(), storageType, poolName, hostname, taskId);
        String refId = this.getProvider(provider).createStoragePool(pool.getId(), storageType, poolName, hostname, taskId);

        pool.setDisplayName(displayName);
        pool.setStorageType(storageType);
        pool.setPoolName(poolName);
        //pool.setProvider(providerType);
        pool.setProvider(provider);
        pool.setProviderRefId(refId);
        if(StorageType.LOCAL.equals(type)){
			pool.setHostname(hostname);
		}
        pool.setDriver(driver.name());
        pool.setConnectProtocol(protocol.getDiskProtocol().value());
        pool.setCreateTime(new Date());

        try {
            this.poolDao.save(pool);
        } catch (DuplicateKeyException dex) {
            log.error("违法唯一性约�?:" + dex, dex);
            throw new GCloudException(StorageErrorCodes.POOL_ALREADY_EXISTS);
        }
        
		StoragePoolZone data = new StoragePoolZone();
		data.setStoragePoolId(pool.getId());
		data.setZoneId(zoneId);
		
		try{
			storagePoolZoneDao.save(data);
		} catch(Exception e) {
			log.error("存储池关联可用区失败，原因：�?"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::存储池关联可用区失败");
		}

        return pool.getId();
    }

    @Override
    public String reportStoragePool(String displayName, Integer providerType, String storageType, String poolName, String categoryCode, String hostname, String driverName)
            throws GCloudException {

        if (ProviderType.get(providerType) == null) {
            throw new GCloudException("provider type " + providerType + " not found");
        }

        StorageType type = StorageType.value(storageType);
        if (type == null) {
            throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
        }

        if (StringUtils.isBlank(poolName)) {
            throw new GCloudException(StorageErrorCodes.INPUT_POOL_NAME_ERROR);
        }
        //存储池暂时�?�过录入，�?�不通过汇报；此汇报仅用于汇报存储池�?关联的节�?
//        StoragePool pool = this.poolDao.find(providerType, storageType, poolName, hostname);
        Map<String, Object> props =new HashMap<String, Object>();
        props.put("provider", providerType);
        props.put("hostname", hostname);
        props.put("poolName", poolName);
        props.put("storageType", storageType);
        StoragePoolNode storagePoolNode = storagePoolNodeDao.findOneByProperties(props);
        if(storagePoolNode == null) {
        	StoragePoolNode addStoragePoolNode = new StoragePoolNode();
        	addStoragePoolNode.setPoolName(poolName);
        	addStoragePoolNode.setProvider(providerType);
        	addStoragePoolNode.setStorageType(storageType);
        	addStoragePoolNode.setHostname(hostname);
        	
        	storagePoolNodeDao.save(addStoragePoolNode);
        }

        /*if (StringUtils.isBlank(displayName)) {
            displayName = hostname + "_local";
        }

        ComputeNode node = null;
        if (StringUtils.isNotBlank(hostname)) {
            node = nodeDao.findUniqueByProperty(ComputeNode.HOSTNAME, hostname);
        }
        if (node == null) {
            throw new GCloudException("node " + hostname + " not found");
        }

        DiskCategory category = this.checkAndGetCategory(node.getZoneId(), categoryCode);

        StoragePoolDriver driver = this.checkAndGetDriver(type, driverName);

        StorageDiskProtocol protocol = StorageDiskProtocol.getByProviderAndStorageType(providerType, driver.name());
        if (protocol == null) {
            throw new GCloudException(StorageErrorCodes.CONNECT_PROTOCOL_NOT_EXISTS);
        }

        StoragePool pool = this.poolDao.find(providerType, storageType, poolName, hostname);
        if (pool != null) {
            if (pool.getDriver() == null || !pool.getDriver().equals(type.getDefaultDriver().name())) {
                pool.setDriver(type.getDefaultDriver().name());
                this.poolDao.update(pool);
            }
        } else {
            pool = new StoragePool();
            pool.setId(StringUtils.genUuid());
            pool.setDisplayName(displayName);
            pool.setStorageType(storageType);
            pool.setPoolName(poolName);
            pool.setProvider(providerType);
            pool.setProviderRefId(pool.getId());
            pool.setHostname(hostname);
            pool.setDriver(driver.name());
            pool.setConnectProtocol(protocol.getDiskProtocol().value());
            try {
                this.poolDao.save(pool);
            } catch (DuplicateKeyException dex) {
                log.error("违法唯一性约�?:" + dex, dex);
                throw new GCloudException(StorageErrorCodes.POOL_ALREADY_EXISTS);
            }
        }*/
        return null;
    }
    
    
    @Transactional
    @Override
    public String reportStoragePool(ReportStoragePoolParams params, String taskId) throws GCloudException {
//    	Integer providerType = params.getProvider();
    	String hostname = params.getHostname();
    	String poolName = params.getPoolName();
    	String storageType = params.getStorageType();

//        if (ProviderType.get(providerType) == null) {
//            throw new GCloudException("provider type " + providerType + " not found");
//        }

        StorageType type = StorageType.value(storageType);
        if (type == null) {
            throw new GCloudException(StorageErrorCodes.NO_SUCH_STORAGE_TYPE);
        }

        if (StringUtils.isBlank(poolName)) {
            throw new GCloudException(StorageErrorCodes.INPUT_POOL_NAME_ERROR);
        }
        
        //查询数据库是否已经存在存储池
        Map<String, Object> queryPoolParams = new HashMap<>();
		queryPoolParams.put(StoragePool.STORAGE_TYPE, storageType);
		queryPoolParams.put(StoragePool.POOL_NAME, poolName);
		queryPoolParams.put(StoragePool.PROVIDER, ProviderType.GCLOUD.getValue());
		queryPoolParams.put(StoragePool.HOSTNAME, hostname);
		List<StoragePool> list = poolDao.findByProperties(queryPoolParams);
		if(null != list && !list.isEmpty()) {
			log.info("[ReportStoragePool]: pool exist");
			throw new GCloudException("::pool exist");
		}
        
        //存储池数据的保存
        String displayName = params.getDisplayName();
        if(StringUtils.isBlank(displayName)) {
			if(StorageType.LOCAL.getValue().equalsIgnoreCase(storageType)) {
				if(StringUtils.isNotBlank(hostname)) {
					displayName = hostname + "_" + poolName + "_" + storageType;
				}
			} else {
				displayName = poolName + "_" + storageType;
			}
		}
        StoragePoolDriver driver = checkAndGetDriver(type, params.getDriver());
		StorageDiskProtocol protocol = StorageDiskProtocol.getByProviderAndStorageType(ProviderType.GCLOUD.getValue(), driver.name());
		if (protocol == null) {
            throw new GCloudException(StorageErrorCodes.CONNECT_PROTOCOL_NOT_EXISTS);
        }
		StoragePool pool = new StoragePool();
		String poolId = StringUtils.genUuid();
		pool.setId(poolId);
		pool.setStorageType(storageType);
		pool.setPoolName(poolName);
		pool.setHostname(hostname);
		pool.setProviderRefId(poolId);
		pool.setCreateTime(new Date());
		pool.setDisplayName(displayName);
		pool.setDriver(driver.name());
		pool.setProvider(ProviderType.GCLOUD.getValue());
		pool.setConnectProtocol(protocol.getDiskProtocol().value());
		try{
			poolDao.saveOrUpdate(pool);
		} catch(Exception e) {
			log.error("[ReportStoragePool]: pool save error, reason -- ["+ e.getMessage() +"]");
			throw new GCloudException(":: pool save error, reason -- ["+ e.getMessage() +"]");
		}
		
		//查询是否存在节点
		Map<String, Object> props = new HashMap<String, Object>();
        props.put(StoragePoolNode.PROVIDER, ProviderType.GCLOUD.getValue());
        props.put(StoragePoolNode.HOSTNAME, hostname);
        props.put(StoragePoolNode.POOL_NAME, poolName);
        props.put(StoragePoolNode.STORAGE_TYPE, storageType);
        StoragePoolNode storagePoolNode = storagePoolNodeDao.findOneByProperties(props);
        if(null != storagePoolNode) {
        	log.info("[ReportStoragePool]: pool node exist");
        	//存储池节点表如果有相同数据，则不�?要动
        	//throw new GCloudException(":: pool node exist");
        } else {
        	//更新或�?�新�? pool node
    		StoragePoolNode addStoragePoolNode = new StoragePoolNode();
        	addStoragePoolNode.setPoolName(poolName);
        	addStoragePoolNode.setProvider(ProviderType.GCLOUD.getValue());
        	addStoragePoolNode.setStorageType(storageType);
        	addStoragePoolNode.setHostname(hostname);
        	try{
        		storagePoolNodeDao.save(addStoragePoolNode);
        	} catch(Exception e) {
        		log.error("[ReportStoragePool]: pool node save error, reason -- ["+ e.getMessage() +"]");
        		throw new GCloudException(":: pool node save error, reason -- ["+ e.getMessage() +"]");
        	}
        }
		
		
    	
    	//存储池可用区表维�?
    	Map<String, Object> nodeParams = new HashMap<>();
    	nodeParams.put(ComputeNode.HOSTNAME, hostname);
    	ComputeNode node = nodeDao.findUniqueByProperties(nodeParams);
    	if(null == node) {
    		log.info("[ReportStoragePool]: node["+ hostname +"] has no zone");
    		//节点没找到可用区，节点可能是刚刚汇报上去还没有关联可用区，不进行下面的存储池关联可用区�?�并且不�?要回�?
    		return null;
//    		throw new GCloudException(":: node has no zone");
    	}

    	//存储池可用区添加
    	Map<String, Object> spzParams = new HashMap<>();
    	spzParams.put(StoragePoolZone.STORAGE_POOL_ID, poolId);
    	List<StoragePoolZone> spzList = storagePoolZoneDao.findByProperties(spzParams);
    	if(spzList != null && !spzList.isEmpty()) {
    		//遇到已经关联了的，先不处理，遇到的几率不�?
    	} else {
    		StoragePoolZone spz = new StoragePoolZone();
    		spz.setStoragePoolId(poolId);
    		spz.setZoneId(node.getZoneId());
    		try{
    			storagePoolZoneDao.saveOrUpdate(spz);
    		} catch(Exception e) {
    			log.error("[ReportStoragePool]: pool zone save error, reason -- ["+ e.getMessage() +"]");
    			throw new GCloudException(":: pool zone save error, reason -- ["+ e.getMessage() +"]");
    		}
    	}
    	
        return null;
    }
    
    private StoragePoolDriver checkAndGetDriver(StorageType type, String driverName) throws GCloudException {
        if (StringUtils.isBlank(driverName)) {
            return type.getDefaultDriver();
        } else {
            StoragePoolDriver driver = StoragePoolDriver.get(driverName);
            if (driver == null) {
                throw new GCloudException(StorageErrorCodes.NO_SUCH_DRIVER);
            }
            return driver;
        }
    }

    @Override
    public void modifyStoragePool(String poolId, String displayName) throws GCloudException {
        synchronized (StoragePoolServiceImpl.class) {
            StoragePool pool = this.poolDao.checkAndGet(poolId);
            if (!StringUtils.equals(pool.getDisplayName(), displayName)) {
                List<String> updateFields = new ArrayList<>();
                updateFields.add(pool.updateDisplayName(displayName));
                this.poolDao.update(pool, updateFields);
                CacheContainer.getInstance().put(CacheType.STORAGEPOOL_NAME, poolId, displayName);
            }
        }
    }

    @Override
    public void deleteStoragePool(String poolId) throws GCloudException {
        synchronized (StoragePoolServiceImpl.class) {
            StoragePool pool = this.poolDao.checkAndGet(poolId);
            
            //判断是否关联了磁盘类�?
            Map<String, Object> dcpzParams = new HashMap<>();
            dcpzParams.put(DiskCategoryPool.STORAGE_POOL_ID, poolId);
            List<DiskCategoryPool> dcpzList = diskCategoryPoolDao.findByProperties(dcpzParams);
            if(dcpzList == null || !dcpzList.isEmpty()) {
            	log.error("::不能删除该存储池，已关联磁盘类型");
            	throw new GCloudException("::不能删除该存储池，已关联磁盘类型");
            }
            
            this.getProvider(pool.getProvider()).deleteStoragePool(pool.getStorageType(), pool.getProviderRefId(), pool.getPoolName());
            this.poolDao.deleteById(poolId);
            
            //删除存储池的时�?�维护存储池可用区表
            storagePoolZoneDao.deleteByPoolId(poolId);
        }
    }
    
    @Override
	public void modifyDiskCategory(ModifyDiskCategoryParams params, CurrentUser currentUser) {
		DiskCategory category = categoryDao.getById(params.getId());
		if(category == null) {
			log.error("::不存在该磁盘类型");
			throw new GCloudException("::不存在该磁盘类型");
		}
		
		Map<String, Object> tmpParams = new HashMap<>();
		tmpParams.put(Volume.CATEGORY, params.getId());
		List<Volume> volumeList = volumeDao.findByProperties(tmpParams);
		if(volumeList != null && !volumeList.isEmpty()) {
			log.error("::不能修改该磁盘类型，已创建相关磁�?");
			throw new GCloudException("::不能修改该磁盘类型，已创建相关磁�?");
		}
		
		List<String> fields = new ArrayList<>();
		fields.add(DiskCategory.MIN_SIZE);
		fields.add(DiskCategory.MAX_SIZE);
		
		category.setMinSize(params.getMinSize() == null ? 0 : params.getMinSize());
		category.setMaxSize(params.getMaxSize() == null ? 0 : params.getMaxSize());
		
		if(category.getMinSize() > category.getMaxSize()) {
			log.error("::磁盘类型的最小�?�不能大于磁盘类型的�?大�??");
			throw new GCloudException("::磁盘类型的最小�?�不能大于磁盘类型的�?大�??");
		}
		
		try{
			categoryDao.update(category, fields);
		} catch(Exception e) {
			log.error("修改磁盘类型失败，原因：�?"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::修改磁盘类型失败");
		}
		
	}

	@Override
	public void deleteDiskCategory(DeleteDiskCategoryParams params, CurrentUser currentUser) {
		DiskCategory category = categoryDao.getById(params.getId());
		if(category == null) {
			log.error("::不存在该磁盘类型");
			throw new GCloudException("::不存在该磁盘类型");
		}
		
		Map<String, Object> tmpParams = new HashMap<>();
		tmpParams.put(Volume.CATEGORY, params.getId());
		List<Volume> volumeList = volumeDao.findByProperties(tmpParams);
		if(volumeList != null && !volumeList.isEmpty()) {
			log.error("::不能删除该磁盘类型，已创建相关磁�?");
			throw new GCloudException("::不能删除该磁盘类型，已创建相关磁�?");
		}
		
		Map<String, Object> poolTmpParams = new HashMap<>();
		poolTmpParams.put(DiskCategoryPool.DISK_CATEGORY_ID, params.getId());
		List<DiskCategoryPool> poolList = diskCategoryPoolDao.findByProperties(poolTmpParams);
		if(poolList != null && !poolList.isEmpty()) {
			log.error("::不能删除该磁盘类型，已关联存储池");
			throw new GCloudException("::不能删除该磁盘类型，已关联存储池");
		}
		
		try{
			diskCategoryZoneDao.deleteByDiskCategoryId(params.getId());
			categoryDao.deleteById(params.getId());
		} catch(Exception e) {
			log.error("删除磁盘类型失败，原因：�?"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::删除磁盘类型失败");
		}
	}

	@Override
	public DiskCategoryModel detailDiskCategory(DetailDiskCategoryParams params, CurrentUser currentUser) {
		DiskCategoryModel response = new DiskCategoryModel();
		List<DiskCategoryInfo> diskCategoryList = categoryDao.detailDiskCategory(params, DiskCategoryInfo.class);

		//将多条数据组合成返回对象
		String categoryId = params.getDiskCategoryId();
		List<DiskCategoryZonePool> associateInfo = new ArrayList<>();
		for (DiskCategoryInfo item : diskCategoryList) {
			if(categoryId.equals(item.getId())) {
				if(null == item.getZoneId()) {
					continue;
				} else {
					DiskCategoryZonePool tmp = new DiskCategoryZonePool();
					tmp.setPoolId(item.getPoolId());
					tmp.setPoolName(item.getPoolName());
					tmp.setHostname(item.getHostname());
					tmp.setZoneId(item.getZoneId());
					tmp.setZoneName(item.getZoneName());
					associateInfo.add(tmp);
				}
			}
		}
		
		//初始化共有的数据
		for (int i = 0; i < diskCategoryList.size(); i++) {
			DiskCategoryInfo item = diskCategoryList.get(i);
			if(categoryId.equals(item.getId())) {
				response.setId(item.getId());
				response.setName(item.getName());
				response.setMaxSize(item.getMaxSize());
				response.setMinSize(item.getMinSize());
				response.setEnabled(item.isEnabled());
				response.setCreateTime(item.getCreateTime());
				response.setStorageType(item.getStorageType());
				break;
			}
		}
		
		response.setAssociateInfo(associateInfo);
		return response;
	}

    private IStoragePoolProvider getProvider(int providerType) {
        IStoragePoolProvider provider = ResourceProviders.checkAndGet(ResourceType.STORAGE_POOL, providerType);
        return provider;
    }

	@Override
	public void associateDiskCategoryZone(AssociateDiskCategoryZoneParams params) {
		String diskCategoryId = params.getDiskCategoryId();
		
		DiskCategory diskCategory = categoryDao.getById(diskCategoryId);
		if(diskCategory == null) {
			log.error("::不存在该磁盘类型");
			throw new GCloudException("::不存在该磁盘类型");
		}
		if(!diskCategory.isEnabled()){
			log.error("::该磁盘类型不可用");
			throw new GCloudException("::该磁盘类型不可用");
		}
		
		//判断参数中的可用区是否有不存在的。�?�辑：根据参数查询是否有相关数据，然后参数去掉查询到的，剩下的就是不存在的了
		List<String> notExist = new ArrayList<>();
		notExist.addAll(params.getZoneIds());
		List<AvailableZoneEntity> zoneList = zoneDao.findByIds(params.getZoneIds());
		List<String> zoneIdsList = new ArrayList<>();
		if(zoneList != null) {
			for (AvailableZoneEntity zone : zoneList) {
				zoneIdsList.add(zone.getId());
			}
		}
		notExist.removeAll(zoneIdsList);
		if(!notExist.isEmpty()) {
			log.error("磁盘类型和可用区关联失败，原因：【可用区"+ notExist.toString() +"不存在�??");
			throw new GCloudException("::不存在可用区 "+ notExist.toString());
		}
		
		//判断参数中的�?要删除的。�?�辑：查询出�?有已经绑定的可用区id，去掉参数的可用区id，剩下的就是�?要删除的
		Map<String, Object> dczParams = new HashMap<>();
		dczParams.put(DiskCategoryZone.DISK_CATEGORY_ID, params.getDiskCategoryId());
		List<DiskCategoryZone> dczList = diskCategoryZoneDao.findByProperties(dczParams);
		
		List<String> dczIdsList = new ArrayList<>();
		if(dczList != null) {
			for (DiskCategoryZone dcz : dczList) {
				dczIdsList.add(dcz.getZoneId());
			}
		}
		List<String> needDelete = new ArrayList<>();
		needDelete.addAll(dczIdsList);
		needDelete.removeAll(params.getZoneIds());
		
		//判断�?要删除中是否关联了存储池，磁盘类型和可用�?
		if(!needDelete.isEmpty()) {
			List<DiskCategoryPool> dcpList = diskCategoryPoolDao.findByDiskCategoryIdAndZoneId(params.getDiskCategoryId(), needDelete);
			if(dcpList != null && !dcpList.isEmpty()) {
				List<String> dcpZoneId = new ArrayList<>();
				List<String> dcpZoneNames = new ArrayList<>();
				for (DiskCategoryPool diskCategoryPool : dcpList) {
					dcpZoneId.add(diskCategoryPool.getZoneId());
				}
				dcpZoneNames = zoneDao.findNamesByIds(dcpZoneId);
				String zoneNames = dcpZoneNames == null ? dcpZoneId.toString() : dcpZoneNames.toString();
				log.error("::磁盘类型已关联存储池，无法取消关联可用区" + zoneNames);
				throw new GCloudException("::磁盘类型已关联存储池，无法取消可用区" + zoneNames);
			}
		}
		
		//判断参数中需要添加的。�?�辑：参数查询出来存在的可用区，去掉已经绑定的，剩下的就是需要绑定的
		List<String> needSave = new ArrayList<>();
		needSave.addAll(zoneIdsList);
		needSave.removeAll(dczIdsList);
		
		//判断可用区的可用�?
		List<String> disableZone = new ArrayList<>();
		for (String zoneId : needSave) {
			for (AvailableZoneEntity zone : zoneList) {
				if(zoneId.equals(zone.getId())) {
					if(!zone.isEnabled()) {
						disableZone.add(zoneId);
					}
					break;
				}
			}
		}
		if(disableZone != null && !disableZone.isEmpty()) {
			List<String> disableZoneNames = zoneDao.findNamesByIds(disableZone);
			String zoneNames = disableZoneNames == null ? disableZone.toString() : disableZoneNames.toString();
			log.error("磁盘类型和可用区关联失败，原因：【可用区 "+ zoneNames +"不可用�??");
			throw new GCloudException("::可用�? "+ zoneNames +"不可�?");
		}
		
		//保存
		List<DiskCategoryZone> saveDczEntitiy = new ArrayList<>();
		if(needSave != null && !needSave.isEmpty()) {
			for (String zoneId : needSave) {
				DiskCategoryZone entity = new DiskCategoryZone();
				entity.setDiskCategoryId(params.getDiskCategoryId());
				entity.setZoneId(zoneId);
				saveDczEntitiy.add(entity);
			}
		}
		
		try{
			diskCategoryZoneDao.saveBatch(saveDczEntitiy);
			if(!needDelete.isEmpty()) {
				diskCategoryZoneDao.deleteByDiskCategoryIdAndZoneId(params.getDiskCategoryId(), needDelete);
			}
		} catch(Exception e) {
			log.error("磁盘类型关联可用区失败，原因：�??"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::磁盘类型关联可用区失�?");
		}
		
	}

	@Override
	public void associateDiskCategoryPool(AssociateDiskCategoryPoolParams params) {
		String diskCategoryId = params.getDiskCategoryId();
		String zoneId = params.getZoneId();
		String poolId = params.getPoolId();
		
		//磁盘类型
		DiskCategory dc = categoryDao.getById(diskCategoryId);
		if(dc == null) {
			log.error("::不存在该磁盘类型");
			throw new GCloudException("::不存在该磁盘类型");
		}
		if(!dc.isEnabled()) {
			log.error("::该磁盘类型不可用");
			throw new GCloudException("::该磁盘类型不可用");
		}
		
		//可用�?
		AvailableZoneEntity zone = zoneDao.getById(zoneId);
		if(zone == null){
			log.error("::不存在该可用�?");
			throw new GCloudException("::不存在该可用�?");
		}
		if(!zone.isEnabled()) {
			log.error("::该可用区不可�?");
			throw new GCloudException("::该可用区不可�?");
		}
		
		//存储�?
		StoragePool sp = poolDao.getById(poolId);
		if(null == sp) {
			log.error("::不存在该存储�?");
			throw new GCloudException("::不存在该存储�?");
		}
		
		//判断磁盘类型是否绑定了可用区
		Map<String, Object> dczParams = new HashMap<>();
		dczParams.put(DiskCategoryZone.DISK_CATEGORY_ID, diskCategoryId);
		dczParams.put(DiskCategoryZone.ZONE_ID, zoneId);
		List<DiskCategoryZone> dczList = diskCategoryZoneDao.findByProperties(dczParams);
		if(dczList == null || dczList.isEmpty()) {
			log.error("::该磁盘类型没有关联该可用�?");
			throw new GCloudException("::该磁盘类型没有关联该可用�?");
		}
		
		//判断存储池是否绑定了可用�?
		Map<String, Object> spzParams = new HashMap<>();
		spzParams.put(StoragePoolZone.STORAGE_POOL_ID, poolId);
		spzParams.put(StoragePoolZone.ZONE_ID, zoneId);
		List<StoragePoolZone> spzList = storagePoolZoneDao.findByProperties(spzParams);
		if(spzList == null || spzList.isEmpty()) {
			log.error("::该存储池没有关联该可用区");
			throw new GCloudException("::该存储池没有关联该可用区");
		}

		//判断是否是同�?个可用区
		DiskCategoryZone dcz = dczList.get(0);
		StoragePoolZone spz = spzList.get(0);
		if(!dcz.getZoneId().equals(spz.getZoneId())) {
			log.error("::无法关联该存储池，不在同�?个可用区");
			throw new GCloudException("::无法关联该存储池，不在同�?个可用区");
		}
		
		//判断是否已经关联�?
		Map<String, Object> dcpParams = new HashMap<>();
		dcpParams.put(DiskCategoryPool.DISK_CATEGORY_ID, dcz.getDiskCategoryId());
		dcpParams.put(DiskCategoryPool.ZONE_ID, dcz.getZoneId());
		dcpParams.put(DiskCategoryPool.STORAGE_POOL_ID, spz.getStoragePoolId());
		List<DiskCategoryPool> dcpList = diskCategoryPoolDao.findByProperties(dcpParams);
		if(dcpList != null && !dcpList.isEmpty()) {
			log.error("::已关联该存储�?");
			throw new GCloudException("::已关联该存储�?");
		}
		
		//判断存储类型是不是一�?
		String dcStorageType = dc.getStorageType();
		String spStorageType = sp.getStorageType();
		if(StringUtils.isBlank(dcStorageType)) {
			log.error("::磁盘类型缺失存储类型");
			throw new GCloudException("::磁盘类型缺失存储类型");
		}
		if(StringUtils.isBlank(spStorageType)) {
			log.error("::存储池缺失存储类�?");
			throw new GCloudException("::存储池缺失存储类�?");
		}
		if(!dcStorageType.equals(spStorageType)) {
			log.error("::磁盘类型与存储池的存储类型不匹配，无法进行关�?");
			throw new GCloudException("::磁盘类型与存储池的存储类型不匹配，无法进行关�?");
		}

		//如果说存储池是本地存储或者是集中存储，磁盘类型与存储池的节点要一�?
		//如果存储池是本地存储类型的话，需要判断同�?个节点是否已经有存储池关联了磁盘类型
		if(StorageType.LOCAL.getValue().equals(spStorageType) || StorageType.CENTRAL.getValue().equals(spStorageType)) {
			String poolHostName = sp.getHostname();
			if(StringUtils.isBlank(poolHostName)) {
				log.error("::本地存储或�?�集中存储的存储池节点不能为�?");
				throw new GCloudException("::本地存储或�?�集中存储的存储池节点不能为�?");
			}
			
			//判断可用区的节点和存储池的节点是否匹�?
			Map<String, Object> nodeParams = new HashMap<>();
			nodeParams.put(ComputeNode.ZONE_ID, zoneId);
			List<ComputeNode> nodeList = nodeDao.findByProperties(nodeParams);
			if(null == nodeList || nodeList.isEmpty()) {
				log.error("::该可用区没有相关节点，无法与本地存储或�?�集中存储的存储池关�?");
				throw new GCloudException("::该可用区没有相关节点，无法与本地存储或�?�集中存储的存储池关�?");
			}
			
			boolean isSameHostName = false;
			for (ComputeNode computeNode : nodeList) {
				if(computeNode.getHostname().equals(poolHostName)) {
					isSameHostName = true;
					break;
				}
			}
			if(!isSameHostName) {
				log.error("::可用区的相关节点与本地存储或者集中存储的存储池的节点不匹�?");
				throw new GCloudException("::可用区的相关节点与本地存储或者集中存储的存储池的节点不匹�?");
			}
			
			
			List<DiskCategoryPool> uniqueDcpList = diskCategoryPoolDao.findWithUniqueHost(diskCategoryId, poolId, zoneId);
			if(null != uniqueDcpList && !uniqueDcpList.isEmpty()) {
				log.error("::同个节点的存储池已经关联了该磁盘类型");
				throw new GCloudException("::同个节点的存储池已经关联了该磁盘类型");
			}
		}
		
		
		//关联
		DiskCategoryPool dcp = new DiskCategoryPool();
		dcp.setDiskCategoryId(dcz.getDiskCategoryId());
		dcp.setZoneId(dcz.getZoneId());
		dcp.setStoragePoolId(spz.getStoragePoolId());
		dcp.setStorageType(sp.getStorageType());
		dcp.setHostname(ObjectUtils.toString(sp.getHostname(), ""));
		
		try{
			diskCategoryPoolDao.save(dcp);
		} catch (DuplicateKeyException dex){
			throw new GCloudException("::此磁盘类型已经关联此存储类型存储�?");
		}catch(Exception e) {
			log.error("关联存储池失败，原因：�??"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::关联存储池失�?");
		}
	}
	
	@Override
	public void associateDiskCategoryMorePool(AssociateDiskCategoryMorePoolParams params) {
		String diskCategoryId = params.getDiskCategoryId();
		String zoneId = params.getZoneId();
		List<String> poolIds = params.getPoolIds();
		
		//磁盘类型
		DiskCategory dc = categoryDao.getById(diskCategoryId);
		if(dc == null) {
			log.error("::不存在该磁盘类型");
			throw new GCloudException("::不存在该磁盘类型");
		}
		if(!dc.isEnabled()) {
			log.error("::该磁盘类型不可用");
			throw new GCloudException("::该磁盘类型不可用");
		}
		
		//可用�?
		AvailableZoneEntity zone = zoneDao.getById(zoneId);
		if(zone == null){
			log.error("::不存在该可用�?");
			throw new GCloudException("::不存在该可用�?");
		}
		if(!zone.isEnabled()) {
			log.error("::该可用区不可�?");
			throw new GCloudException("::该可用区不可�?");
		}
		
		//判断磁盘类型是否绑定了可用区
		Map<String, Object> dczParams = new HashMap<>();
		dczParams.put(DiskCategoryZone.DISK_CATEGORY_ID, diskCategoryId);
		dczParams.put(DiskCategoryZone.ZONE_ID, zoneId);
		List<DiskCategoryZone> dczList = diskCategoryZoneDao.findByProperties(dczParams);
		if(dczList == null || dczList.isEmpty()) {
			log.error("::该磁盘类型没有关联该可用�?");
			throw new GCloudException("::该磁盘类型没有关联该可用�?");
		}
		
		//存储池绑定了可用区的列表
		List<StoragePoolZone> poolZoneList = new ArrayList<>();
		
		//判断存储池是否绑定了可用�?
		//TODO poolId可能会有很多，根据Ids查询可能会出错，现在这种查询时间估计很慢待优�?
		List<String> noAssciatePoolNames = new ArrayList<>();
		for (String poolId : poolIds) {
			Map<String, Object> spzParams = new HashMap<>();
			spzParams.put(StoragePoolZone.STORAGE_POOL_ID, poolId);
			spzParams.put(StoragePoolZone.ZONE_ID, zoneId);
			List<StoragePoolZone> spzList = storagePoolZoneDao.findByProperties(spzParams);
			if(spzList == null || spzList.isEmpty()) {
				String poolName = CacheContainer.getInstance().getString(CacheType.STORAGEPOOL_NAME, poolId);
				noAssciatePoolNames.add(StringUtils.isNotBlank(poolName) ? poolName : poolId);
			} else {
				poolZoneList.add(spzList.get(0));
			}
		}
		if(!noAssciatePoolNames.isEmpty()) {
			log.error("磁盘类型和存储池关联失败，原因：【可用区没关联存储池"+ noAssciatePoolNames.toString() +"�?");
			throw new GCloudException("::可用区没关联存储�? "+ noAssciatePoolNames.toString());
		}
		
		
		List<StoragePool> pools = new ArrayList<>();
		//存储�?
		//判断参数中的存储池是否有不存在的。�?�辑：根据参数查询是否有相关数据，然后参数去掉查询到的，剩下的就是不存在的了
		//TODO poolId可能会有很多，根据Ids查询可能会出错，现在这种查询时间估计很慢待优�?
		List<String> notExist = new ArrayList<>();
		notExist.addAll(params.getPoolIds());
		List<String> poolIdList = new ArrayList<>();
		for (String poolId : poolIds) {
			StoragePool sp = poolDao.getById(poolId);
			if(null != sp) {
				poolIdList.add(sp.getId());
				pools.add(sp);
			}
		}
		notExist.removeAll(poolIdList);
		if(!notExist.isEmpty()) {
			log.error("磁盘类型和存储池关联失败，原因：【存储池"+ notExist.toString() +"不存在�??");
			throw new GCloudException("::不存在存储池 "+ notExist.toString());
		}
		
		//判断参数中的�?要删除的。�?�辑：查询出�?有已经绑定的可用区id，去掉参数的可用区id，剩下的就是�?要删除的
		Map<String, Object> dcpFindParams = new HashMap<>();
		dcpFindParams.put(DiskCategoryPool.DISK_CATEGORY_ID, params.getDiskCategoryId());
		dcpFindParams.put(DiskCategoryPool.ZONE_ID, params.getZoneId());
		List<DiskCategoryPool> dcpList = diskCategoryPoolDao.findByProperties(dcpFindParams);
		Set<String> existsPools = new HashSet<>();
		
		List<String> dcpIdsList = new ArrayList<>();
		if(dcpList != null) {
			for (DiskCategoryPool dcp : dcpList) {
				dcpIdsList.add(dcp.getStoragePoolId());
				existsPools.add(String.format("%s,%s,%s", dcp.getZoneId(), dcp.getStorageType(), dcp.getHostname()));
			}
		}
		List<String> needDelete = new ArrayList<>();
		needDelete.addAll(dcpIdsList);
		needDelete.removeAll(params.getPoolIds());
		
		//TODO 解绑是否有什么限�?
		if(!needDelete.isEmpty()) {
		}
		
		//判断参数中需要添加的。�?�辑：参数查询出来存在的可用区，去掉已经绑定的，剩下的就是需要绑定的
		List<String> needSave = new ArrayList<>();
		needSave.addAll(poolIdList);
		needSave.removeAll(dcpIdsList);
		
		//判断是否是同�?个可用区
		DiskCategoryZone dcz = dczList.get(0);
		List<String> noZonePoolNames = new ArrayList<>();
		for (StoragePoolZone item : poolZoneList) {
			String poolName = CacheContainer.getInstance().getString(CacheType.STORAGEPOOL_NAME, item.getStoragePoolId());
			String poolNameOrId = (StringUtils.isNotBlank(poolName) ? poolName : item.getStoragePoolId());
			
			//判断是否是同�?个可用区
			if(!dcz.getZoneId().equals(item.getZoneId())) {
				noZonePoolNames.add(poolNameOrId);
			}
		}
		//判断是否是同�?个可用区
		if(!noZonePoolNames.isEmpty()) {
			log.error("::不在同一个可用区，无法关联存储池" + noZonePoolNames.toString());
			throw new GCloudException("::不在同一个可用区，无法关联存储池" + noZonePoolNames.toString());
		}
		
		//判断存磁盘类型的存储类型
		String dcStorageType = dc.getStorageType();
		if(StringUtils.isBlank(dcStorageType)) {
			log.error("::磁盘类型缺失存储类型");
			throw new GCloudException("::磁盘类型缺失存储类型");
		}
		
		//判断可用区对应的节点,用于后面判断可用区节点和存储池的节点是否匹配
		Map<String, Object> nodeParams = new HashMap<>();
		nodeParams.put(ComputeNode.ZONE_ID, zoneId);
		List<ComputeNode> nodeList = nodeDao.findByProperties(nodeParams);
		if(null == nodeList || nodeList.isEmpty()) {
			log.error("::该可用区没有相关节点，无法与本地存储或�?�集中存储的存储池关�?");
			throw new GCloudException("::该可用区没有相关节点，无法与本地存储或�?�集中存储的存储池关�?");
		}



		Map<String, StoragePool> poolMap = new HashMap<>();

		//校验
		//TODO 数据库操作过多，待优�?
		if(needSave != null && !needSave.isEmpty()) {
			List<String> lostStorageTypePoolName = new ArrayList<>();
			List<String> noMatchStorageTypePoolName = new ArrayList<>();
			
			Iterator<String> it = needSave.iterator();
			while(it.hasNext()) {
				String poolId = it.next();
				StoragePool sp = poolDao.getById(poolId);
				poolMap.put(poolId, sp);
				
				//校验存储池的存储类型和磁盘类型的存储类型是否匹配
				String spStorageType = sp.getStorageType();
				if(StringUtils.isBlank(spStorageType)) {
					lostStorageTypePoolName.add(sp.getDisplayName());
				}
				if(!dcStorageType.equals(spStorageType)) {
					noMatchStorageTypePoolName.add(sp.getDisplayName());
				}

				if(!existsPools.add(String.format("%s,%s,%s", zoneId, sp.getStorageType(), sp.getHostname()))) {
					log.error("::同个节点的存储池已经关联了该磁盘类型�? 存储�?" + sp.getDisplayName());
					throw new GCloudException("::同个节点的存储池已经关联了该磁盘类型�? 存储�?" + sp.getDisplayName());
				}
				
//				//如果说存储池是本地存储或者是集中存储，磁盘类型与存储池的节点要一�?
//				//如果存储池是本地存储类型的话，需要判断同�?个节点是否已经有存储池关联了磁盘类型
				if(StorageType.LOCAL.getValue().equals(spStorageType)) {
					List<String> hostnameNotNullLocalPoolName = new ArrayList<>();
					List<String> noMatchHostPoolName = new ArrayList<>();
					List<String> uniqueLocalPoolName = new ArrayList<>();
					
					String poolHostName = sp.getHostname();
					if(StringUtils.isBlank(poolHostName)) {
						hostnameNotNullLocalPoolName.add(sp.getDisplayName());
					}

					//判断可用区的节点和存储池的节点是否匹�?
					boolean isSameHostName = false;
					for (ComputeNode computeNode : nodeList) {
						if(computeNode.getHostname().equals(poolHostName)) {
							isSameHostName = true;
							break;
						}
					}
					if(!isSameHostName) {
						noMatchHostPoolName.add(sp.getDisplayName());
					}
					
					if(!hostnameNotNullLocalPoolName.isEmpty()) {
						log.error("::本地存储的节点不能为空， 存储�?" + hostnameNotNullLocalPoolName.toString());
						throw new GCloudException("::本地存储的节点不能为空， 存储�?" + hostnameNotNullLocalPoolName.toString());
					}
					if(!noMatchHostPoolName.isEmpty()) {
						log.error("::可用区的相关节点与本地存储或者集中存储的存储池的节点不匹配，存储�?" + noMatchHostPoolName.toString());
						throw new GCloudException("::可用区的相关节点与本地存储或者集中存储的存储池的节点不匹配，存储�?" + noMatchHostPoolName.toString());
					}

				}	//判断本地和集中存储的相关校验结束
			}
			if(!lostStorageTypePoolName.isEmpty()) {
				log.error("::缺失存储类型，存储池" + lostStorageTypePoolName.toString());
				throw new GCloudException("::缺失存储类型，存储池" + lostStorageTypePoolName.toString());
			}
			if(!noMatchStorageTypePoolName.isEmpty()) {
				log.error("::磁盘类型存储类型不匹配，存储�?" + noMatchStorageTypePoolName.toString());
				throw new GCloudException("::磁盘类型存储类型不匹配，存储�?" + noMatchStorageTypePoolName.toString());
			}
		}
		
		//关联
		List<DiskCategoryPool> saveDcpEntity = new ArrayList<>();
		if(needSave != null && !needSave.isEmpty()) {
			for (String poolId : needSave) {
				StoragePool sp = poolMap.get(poolId);
				DiskCategoryPool dcp = new DiskCategoryPool();
				dcp.setDiskCategoryId(dcz.getDiskCategoryId());
				dcp.setZoneId(dcz.getZoneId());
				dcp.setStoragePoolId(poolId);
				dcp.setStorageType(sp.getStorageType());
				dcp.setHostname(ObjectUtils.toString(sp.getHostname(), ""));
				saveDcpEntity.add(dcp);
			}
		}
		
		try{
			diskCategoryPoolDao.saveBatch(saveDcpEntity);
			if(!needDelete.isEmpty()) {
				diskCategoryPoolDao.deleteBatchByPoolId(dcz.getDiskCategoryId(), dcz.getZoneId(), needDelete);
			}
		} catch(Exception e) {
			log.error("关联存储池失败，原因：�??"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::关联存储池失�?");
		}
	}

	@Override
	public ApiPoolsStatisticsReplyMsg poolStatistics(String poolId) {
		/*ApiPoolsStatisticsReplyMsg reply = new ApiPoolsStatisticsReplyMsg();
		List<StoragePool> pools = new ArrayList<StoragePool>();
		if(StringUtils.isNotBlank(poolId)) {
			StoragePool storagePool = this.poolDao.checkAndGet(poolId);
			pools.add(storagePool);
            
		} else {
			pools = poolDao.findAll();
		}
		for(StoragePool pool:pools) {
			StoragePoolInfo info = this.getProvider(pool.getProvider()).getPoolSize(pool);
			if(info == null) {
				continue;
			}
            reply.setPoolNum(reply.getPoolNum() + 1);
            reply.setPoolTotalSize(reply.getPoolTotalSize() + info.getTotalSize());
            reply.setPoolAvailSize(reply.getPoolAvailSize() + info.getAvailSize());
            reply.setPoolUsedSize(reply.getPoolUsedSize() + info.getUsedSize());
		}*
		return reply;*/
		String batchId = storagePoolCollectCronTaskDao.getLastedTask();
		
		return storagePoolCollectDetailDao.statistic(batchId==null?"":batchId, ApiPoolsStatisticsReplyMsg.class);
	}


	@Override
	public StoragePool getPoolById(String poolId) {
		return poolDao.getById(poolId);
	}


	@Override
	public void associatePoolZone(AssociatePoolZoneParams params, CurrentUser currentUser) {
		String poolId = params.getPoolId();
		
		StoragePool pool = poolDao.getById(poolId);
		if(null == pool) {
			log.error("::不存在该存储�?");
			throw new GCloudException("::不存在该存储�?");
		}
		
		//判断参数中的可用区是否有不存在的。�?�辑：根据参数查询是否有相关数据，然后参数去掉查询到的，剩下的就是不存在的了
		List<String> notExist = new ArrayList<>();
		notExist.addAll(params.getZoneIds());
		List<AvailableZoneEntity> zoneList = zoneDao.findByIds(params.getZoneIds());
		List<String> zoneIdsList = new ArrayList<>();
		if(zoneList != null) {
			for (AvailableZoneEntity zone : zoneList) {
				zoneIdsList.add(zone.getId());
			}
		}
		notExist.removeAll(zoneIdsList);
		if(!notExist.isEmpty()) {
			log.error("存储池和可用区关联失败，原因：�?�可用区"+ notExist.toString() +"不存在�??");
			throw new GCloudException("::不存在可用区 "+ notExist.toString());
		}
		
		//判断参数中的�?要删除的。�?�辑：查询出�?有已经绑定的可用区id，去掉参数的可用区id，剩下的就是�?要删除的
		Map<String, Object> spzParams = new HashMap<>();
		spzParams.put(StoragePoolZone.STORAGE_POOL_ID, params.getPoolId());
		List<StoragePoolZone> spzList = storagePoolZoneDao.findByProperties(spzParams);
		
		List<String> spzIdsList = new ArrayList<>();
		if(spzList != null) {
			for (StoragePoolZone spz : spzList) {
				spzIdsList.add(spz.getZoneId());
			}
		}
		List<String> needDelete = new ArrayList<>();
		needDelete.addAll(spzIdsList);
		needDelete.removeAll(params.getZoneIds());
		
		//判断参数中需要添加的。�?�辑：参数查询出来存在的可用区，去掉已经绑定的，剩下的就是需要绑定的
		List<String> needSave = new ArrayList<>();
		needSave.addAll(zoneIdsList);
		needSave.removeAll(spzIdsList);
		
		//判断�?要删除中是否关联了存储池，磁盘类型和可用�?
		if(!needDelete.isEmpty()) {
			List<DiskCategoryPool> dcpList = diskCategoryPoolDao.findByPoolIdAndZoneId(params.getPoolId(), needDelete);
			if(dcpList != null && !dcpList.isEmpty()) {
				List<String> dcpZoneId = new ArrayList<>();
				List<String> dcpZoneNames = new ArrayList<>();
				for (DiskCategoryPool diskCategoryPool : dcpList) {
					dcpZoneId.add(diskCategoryPool.getZoneId());
				}
				dcpZoneNames = zoneDao.findNamesByIds(dcpZoneId);
				String zoneNames = dcpZoneNames == null ? dcpZoneId.toString() : dcpZoneNames.toString();
				log.error("::磁盘类型已关联存储池，无法取消关联可用区" + zoneNames);
				throw new GCloudException("::磁盘类型已关联存储池，无法取消可用区" + zoneNames);
			}
		}
		
		//判断可用区的可用�?
		List<String> disableZone = new ArrayList<>();
		for (String zoneId : needSave) {
			for (AvailableZoneEntity zone : zoneList) {
				if(zoneId.equals(zone.getId())) {
					if(!zone.isEnabled()) {
						disableZone.add(zoneId);
					}
					break;
				}
			}
		}
		if(disableZone != null && !disableZone.isEmpty()) {
			List<String> disableZoneNames = zoneDao.findNamesByIds(disableZone);
			String zoneNames = disableZoneNames == null ? disableZone.toString() : disableZoneNames.toString();
			log.error("存储池和可用区关联失败，原因：�?�可用区 "+ zoneNames +"不可用�??");
			throw new GCloudException("::可用�? "+ zoneNames +"不可�?");
		}
		
		//校验存储池节点对应的可用区和传参的可用区是否�?�?
		if(StringUtils.isNotBlank(pool.getHostname())) {
			PageResult<AvailableZoneEntity> page = zoneDao.findPageByHostname(pool.getHostname(), 1, -1, AvailableZoneEntity.class);
			//存储池对应的可用�?
			List<String> zoneIds = new ArrayList<>();
			List<String> errorZoneIds = new ArrayList<>();
			boolean errorFlag = true;
			
			if(page.getList() == null || page.getList().isEmpty()) {
				log.error("::节点["+ pool.getHostname() +"] 关联的可用区异常");
				throw new GCloudException("::节点["+ pool.getHostname() +"] 关联的可用区异常");
			}
			for (AvailableZoneEntity item : page.getList()) {
				zoneIds.add(item.getId());
			}
			for (String id : needSave) {
				if(zoneIds.contains(id)) {
					errorFlag = false;
				} else {
					errorZoneIds.add(id);
				}
			}
			
			if(errorFlag) {
				List<String> errorZoneNames = zoneDao.findNamesByIds(errorZoneIds);
				String zoneNames = errorZoneNames == null ? errorZoneIds.toString() : errorZoneNames.toString();
				log.error("::存储池无法关联可用区" + zoneNames);
				throw new GCloudException("::存储池无法关联可用区" + zoneNames);
			}
			
		}
			
		//保存
		List<StoragePoolZone> saveZitEntitiy = new ArrayList<>();
		if(needSave != null && !needSave.isEmpty()) {
			for (String zoneId : needSave) {
				StoragePoolZone entity = new StoragePoolZone();
				entity.setStoragePoolId(params.getPoolId());
				entity.setZoneId(zoneId);
				saveZitEntitiy.add(entity);
			}
		}
		
		try{
			storagePoolZoneDao.saveBatch(saveZitEntitiy);
			if(!needDelete.isEmpty()) {
				storagePoolZoneDao.deleteByPoolIdAndZoneId(params.getPoolId(), needDelete);
			}
		} catch(Exception e) {
			log.error("存储池关联可用区失败，原因：�?"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::存储池关联可用区失败");
		}
	}


	@Override
	public void enableDiskCategory(EnableDiskCategoryParams params, CurrentUser currentUser) {
		DiskCategory diskCategory = categoryDao.getById(params.getDiskCategoryId());
		if(null == diskCategory) {
			log.error("::不存在该磁盘类型");
			throw new GCloudException("::不存在该磁盘类型");
		}
		
		boolean flag = params.isEnable();
		boolean currentEnable = diskCategory.isEnabled();
		
		//启用
		if(flag) {
			if(currentEnable) {
				return;
			} else {
				diskCategory.setEnabled(true);
			}
		} 
		//禁用
		else {
			if(!currentEnable) {
				return;
			} else {
				diskCategory.setEnabled(false);
			}
		}
		
		try{
			categoryDao.update(diskCategory);
		} catch(Exception e) {
			log.error("启用或禁用磁盘类型失败，原因：�??"+ e.getCause() + "::" + e.getCause() +"�?");
			throw new GCloudException("::启用或禁用磁盘类型失�?");
		}
		
	}


	@Override
	public StoragePoolType deatilPool(DetailPoolParams params, CurrentUser currentUser) {
		StoragePoolType response = new StoragePoolType();
		List<StoragePoolDetailInfo> data = poolDao.detailPool(params, currentUser, StoragePoolDetailInfo.class);
		
		String poolId = params.getPoolId();
		List<StoragePoolZoneInfo> associateInfo = new ArrayList<>();
		
		//将多条数据组合成返回对象
		for (StoragePoolDetailInfo item : data) {
			if(poolId.equals(item.getId())) {
				if(null == item.getZoneId()) {
					continue;
				} else {
					StoragePoolZoneInfo tmp = new StoragePoolZoneInfo();
					tmp.setZoneId(item.getZoneId());
					tmp.setZoneName(item.getZoneName());
					associateInfo.add(tmp);
				}
			}
		}
		
		//初始化公有的数据
		for (int i = 0; i < data.size(); i++) {
			StoragePoolDetailInfo item = data.get(i);
			if(poolId.equals(item.getId())) {
				response.setId(item.getId());
				response.setDisplayName(item.getDisplayName());
				response.setProvider(item.getProvider());
				response.setStorageType(item.getStorageType());
				response.setPoolName(item.getPoolName());
				response.setHostname(item.getHostname());
				response.setDriver(item.getDriver());
				response.setCreateTime(item.getCreateTime());
			}
		}
		response.setAssociateInfo(associateInfo);
		return response;
	}


	@Override
	public void unassociateDiskCategoryPool(UnassociateDiskCategoryPoolParams params) {
		String diskCategoryId = params.getDiskCategoryId();
		String zoneId = params.getZoneId();
		String poolId = params.getPoolId();
		
		DiskCategory dc = categoryDao.getById(diskCategoryId);
		if(dc == null) {
			log.error("::不存在该磁盘类型");
			throw new GCloudException("::不存在该磁盘类型");
		}
		if(!dc.isEnabled()) {
			log.error("::该磁盘类型不可用");
			throw new GCloudException("::该磁盘类型不可用");
		}
		
		AvailableZoneEntity zone = zoneDao.getById(zoneId);
		if(zone == null){
			log.error("::不存在该可用�?");
			throw new GCloudException("::不存在该可用�?");
		}
		if(!zone.isEnabled()) {
			log.error("::该可用区不可�?");
			throw new GCloudException("::该可用区不可�?");
		}
		
		//判断磁盘类型是否绑定了可用区
		Map<String, Object> dczParams = new HashMap<>();
		dczParams.put(DiskCategoryZone.DISK_CATEGORY_ID, diskCategoryId);
		dczParams.put(DiskCategoryZone.ZONE_ID, zoneId);
		List<DiskCategoryZone> dczList = diskCategoryZoneDao.findByProperties(dczParams);
		if(dczList == null || dczList.isEmpty()) {
			log.error("::该磁盘类型没有关联该可用�?");
			throw new GCloudException("::该磁盘类型没有关联该可用�?");
		}
		
		//判断存储池是否绑定了可用�?
		Map<String, Object> spzParams = new HashMap<>();
		spzParams.put(StoragePoolZone.STORAGE_POOL_ID, poolId);
		spzParams.put(StoragePoolZone.ZONE_ID, zoneId);
		List<StoragePoolZone> spzList = storagePoolZoneDao.findByProperties(spzParams);
		if(spzList == null || spzList.isEmpty()) {
			log.error("::该存储池没有关联该可用区");
			throw new GCloudException("::该存储池没有关联该可用区");
		}

		Map<String, Object> dcpParams = new HashMap<>();
		dcpParams.put(DiskCategoryPool.DISK_CATEGORY_ID, params.getDiskCategoryId());
		dcpParams.put(DiskCategoryPool.ZONE_ID, params.getZoneId());
		dcpParams.put(DiskCategoryPool.STORAGE_POOL_ID, params.getPoolId());
		List<DiskCategoryPool> dcpList = diskCategoryPoolDao.findByProperties(dcpParams);
		if(dcpList == null || dcpList.isEmpty()) {
			log.error("::磁盘类型没有关联存储�?");
			throw new GCloudException("::磁盘类型没有关联存储�?");
		}
		
		List<Integer> dcpIds = new ArrayList<>();
		for (DiskCategoryPool dcp : dcpList) {
			dcpIds.add(dcp.getId());
		}
		
		try{
			if(!dcpIds.isEmpty()) {
				diskCategoryPoolDao.deleteByDcpIds(dcpIds);
			}
		} catch(Exception e) {
			log.error("关联存储池失败，原因：�??"+ e.getCause() + "::" + e.getMessage() +"�?");
			throw new GCloudException("::关联存储池失�?");
		}
	}


	@Override
	public DiskCategory getDiskCategoryById(String diskCategoryId) {
		if(StringUtils.isBlank(diskCategoryId)) {
			return null;
		}
		return categoryDao.getById(diskCategoryId);
	}

	@Override
	public List<String> categoryHost(String category, String zoneId) {
		return categoryHost(categoryDao.getById(category), zoneId);
	}

	@Override
	public List<String> categoryHost(DiskCategory category, String zoneId) {

		List<String> hosts = null;
		//本地的系统盘
		if(StorageType.LOCAL.getValue().equals(category.getStorageType())){
			hosts = categoryDao.diskCategoryHost(category.getId(), zoneId, true);
		}else{
			hosts = nodeDao.zoneNode(zoneId, true);
		}

		return hosts;
	}

	@Override
	public StoragePool assignStoragePool(String category, String zoneId, String createHost) {
		return assignStoragePool(categoryDao.getById(category), zoneId, createHost);
	}

	@Override
	public StoragePool assignStoragePool(DiskCategory category, String zoneId, String createHost) {

    	//暂时只有本地存储才需要hostname
    	if(!category.getStorageType().equals(StorageType.LOCAL.getValue())){
			createHost = null;
		}

		//TODO 以后�?要扩展存储池选策略，改为返回列表
		return categoryDao.diskCategoryStoragePool(category.getId(), zoneId, createHost, null);
	}


	@Override
	public void updateLocalPoolZone(String hostName, String zoneId) {
		//节点关联了可用区,更新已有的存储池可用区表
		storagePoolZoneDao.updateZoneByPoolHostname(hostName, zoneId);
        
        //添加没有关联可用区的存储�?
        List<StoragePool> poolList = poolDao.findPoolWithoutZoneByHostname(hostName);
        if(null != poolList && !poolList.isEmpty()) {
        	List<StoragePoolZone> poolZones = new ArrayList<>();
        	for (StoragePool pool : poolList) {
        		StoragePoolZone poolZone = new StoragePoolZone();
        		poolZone.setStoragePoolId(pool.getId());
        		poolZone.setZoneId(zoneId);
        		poolZones.add(poolZone);
			}
        	storagePoolZoneDao.saveBatch(poolZones);
        }
		
	}
	
	@Override
	public void clearLocalPoolZone(String hostName) {
		storagePoolZoneDao.deleteByHostname(hostName);
	}
	
	/**
	 * 当新批次产生时，上一批次就视为完成收�?
	 * 每次只保留当前收集中的批次与上一次完成的批次数据
	 * @param batchId
	 */
	private void createNewCollectStorageInfoCron(String batchId) {
		//分布式锁
		String lockName = StorageControllerUtil.getPoolInfoCollectCronLock(batchId);
        String lockid = "";
        
        lockid = LockUtil.spinLock(lockName, Consts.Time.STORAGE_REDIS_POOL_INFO_COLLECT_CRON_LOCK_TIMEOUT, Consts.Time.STORAGE_REDIS_POOL_INFO_COLLECT_CRON_LOCK_GET_LOCK_TIMEOUT, String.format("::获取存储池信息收集批次锁失败,batchId:%s", batchId));
		try {
			StoragePoolCollectCronTask storagePoolCollectCronTask = new StoragePoolCollectCronTask();
			storagePoolCollectCronTask.setBatchId(batchId);
			storagePoolCollectCronTask.setCreateTime(new Date());
			storagePoolCollectCronTask.setState(CollectStorageInfoState.COLLECTING.value());//CollectStorageInfoState.COLLECTING.value()
			storagePoolCollectCronTaskDao.save(storagePoolCollectCronTask);
			
			//删掉之前收集完成的数�?
			storagePoolCollectDetailDao.deleteHistoryBatchData();
			storagePoolCollectCronTaskDao.deleteHistoryBatch();
			
			//将之前批次的设置为收集完�?
			storagePoolCollectCronTaskDao.setPreTaskCollected(batchId);
		}catch(Exception ex) {
			log.debug(String.format("%s批次已存�?", batchId));
		}finally {
            LockUtil.releaseSpinLock(lockName, lockid);
        }
	}

	@Override
	public void initCollectStorageInfoCron(String batchId) {
		createNewCollectStorageInfoCron(batchId);
		
		/*暂时不记录，
		 * List<StoragePool> pools = poolDao.findAll();
		 Map<String, Node> onlineNodes = RedisNodesUtil.getComputeNodes();
		for(StoragePool pool:pools) {
			try {
				if(!pool.getStorageType().equals(StorageType.LOCAL.getValue()) 
						|| (pool.getStorageType().equals(StorageType.LOCAL.getValue()) && onlineNodes.containsKey(pool.getHostname()))) {
					StoragePoolCollectDetail detail = new StoragePoolCollectDetail();
					detail.setBatchId(batchId);
					detail.setPoolId(pool.getId());
					detail.setState(CollectStorageInfoState.COLLECTING.value());
					detail.setCreateTime(new Date());
					
					storagePoolCollectDetailDao.save(detail);
				}
			}catch(Exception ex) {
				log.debug(String.format("%s批次对应的池%s已存�?", batchId, pool.getId()));
			}
		}*/
	}


	@Override
	public void collectStorageInfos(String batchId, String[] storageTypes) {
		List<StoragePool> storagePools = poolDao.getStoragePoolsByTypes(storageTypes);
		for(StoragePool pool:storagePools) {
			StoragePoolInfo info = this.getProvider(pool.getProvider()).getPoolSize(pool);
			if(info == null) {
				continue;
			}
			
			Map<String, Object> detailProps = new HashMap<String, Object>();
			detailProps.put("batchId", batchId);
			detailProps.put("poolId", pool.getId());
			StoragePoolCollectDetail detail = storagePoolCollectDetailDao.findUniqueByProperties(detailProps);
			if(detail == null) {
				detail = new StoragePoolCollectDetail();
				detail.setBatchId(batchId);
				detail.setPoolId(pool.getId());
				detail.setCreateTime(new Date());
			}
			
			detail.setState(CollectStorageInfoState.COLLECTED.value());
			detail.setAvail(info.getAvailSize());
			detail.setUsed(info.getUsedSize());
			detail.setTotal(info.getTotalSize());
			
			storagePoolCollectDetailDao.saveOrUpdate(detail);
			
		}
	}


	@Override
	public void reportLocalPool(LocalPoolReportMsg msg) {
		StoragePoolCollectCronTask task = storagePoolCollectCronTaskDao.getById(msg.getCron());
		if(task == null) {
			createNewCollectStorageInfoCron(msg.getCron());
		}
		
		for(LocalPoolReport pool:msg.getRes().getPools()) {
			try {
				Map<String, Object> props = new HashMap<String, Object>();
				props.put("hostname", pool.getHostname());
				props.put("poolName", pool.getPoolName());
				StoragePool storagePool = poolDao.findUniqueByProperties(props);
				
				if(storagePool==null) {
					log.error(String.format("hostname:%s,poolName:%s �?对应的本地存储池不存�?", pool.getHostname(), pool.getPoolName()));
					throw new GCloudException("::池不存在");
				}
				
				Map<String, Object> detailProps = new HashMap<String, Object>();
				detailProps.put("batchId", msg.getCron());
				detailProps.put("poolId", storagePool.getId());
				StoragePoolCollectDetail detail = storagePoolCollectDetailDao.findUniqueByProperties(detailProps);
				if(detail == null) {
					detail = new StoragePoolCollectDetail();
					detail.setBatchId(msg.getCron());
					detail.setPoolId(storagePool.getId());
					detail.setCreateTime(new Date());
				}
				
				detail.setState(CollectStorageInfoState.COLLECTED.value());
				detail.setAvail(pool.getAvail());
				detail.setUsed(pool.getUsed());
				detail.setTotal(pool.getTotal());
				
				storagePoolCollectDetailDao.saveOrUpdate(detail);
			}catch(Exception ex) {
				log.error(String.format("%s批次，池hostname:%s,poolName:%s保存失败", msg.getCron(), pool.getHostname(), pool.getPoolName()));
			}
		}
	}

	@Override
	public List<StorageTypeVo> diskCategoryStorageType() {
		return storageTypeVo(categoryDao.diskCategoryStorageType());
	}

	@Override
	public List<StorageTypeVo> storagePoolStorageType() {
		return storageTypeVo(poolDao.storagePoolStorageType());
	}

	private List<StorageTypeVo> storageTypeVo(List<String> storageTypes){

    	if(storageTypes == null){
    		return null;
		}
    	if(storageTypes.size() == 0){
    		return new ArrayList<>();
		}

    	Map<String, String> storageTypeNameMap = new HashMap<>();
		Arrays.stream(StorageType.values()).forEach(s -> storageTypeNameMap.put(s.getValue(), s.getCnName()));
    	List<StorageTypeVo> storageTypeVos = new ArrayList<>();
    	for(String type : storageTypes){
    		StorageTypeVo vo = new StorageTypeVo();
    		vo.setStorageType(type);
    		vo.setCnName(storageTypeNameMap.get(type));
			storageTypeVos.add(vo);
		}

    	return storageTypeVos;
	}
}