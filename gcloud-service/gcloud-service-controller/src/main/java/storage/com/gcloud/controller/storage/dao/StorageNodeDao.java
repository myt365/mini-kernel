package com.gcloud.controller.storage.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.storage.entity.StorageNode;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Jdbc("controllerJdbcTemplate")
@Repository
public class StorageNodeDao extends JdbcBaseDaoImpl<StorageNode, Integer>{
	public void updateBatch(String whereField, List<String> ids, String setField, Object value) {
		if (ids.size() == 0) {
			return;
		}
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE gc_storage_nodes SET `");
		sb.append(setField);
		sb.append("` = '");
		sb.append(value);
		sb.append("'  WHERE ");
		int l = ids.size();
		Object[] os = new Object[l];
		for (int i = 0; i < l; i++) {
			if (i > 0) {
				sb.append(" OR ");
			}
			sb.append(" `");
			sb.append(whereField);
			sb.append("` = ? ");

			os[i] = ids.get(i);
		}
		jdbcTemplate.update(sb.toString(), os);
	}
}