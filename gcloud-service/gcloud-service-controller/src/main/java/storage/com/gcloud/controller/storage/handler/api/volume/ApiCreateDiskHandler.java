package com.gcloud.controller.storage.handler.api.volume;

import com.gcloud.controller.storage.model.CreateDiskParams;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.storage.msg.api.volume.ApiCreateDiskMsg;
import com.gcloud.header.storage.msg.api.volume.ApiCreateDiskReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@LongTask
@GcLog(taskExpect = "创建磁盘")
@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="CreateDisk")
public class ApiCreateDiskHandler extends MessageHandler<ApiCreateDiskMsg, ApiCreateDiskReplyMsg> {

    @Autowired
    private IVolumeService volumeService;

    @Override
    public ApiCreateDiskReplyMsg handle(ApiCreateDiskMsg msg) throws GCloudException {

        CreateDiskParams params = BeanUtil.copyProperties(msg, CreateDiskParams.class);
        params.setTaskId(msg.getTaskId());
        String volumeId = volumeService.create(params, msg.getCurrentUser());
        ApiCreateDiskReplyMsg reply = new ApiCreateDiskReplyMsg();
        
        //添加操作日志的对象名�?
        msg.setObjectId(volumeId);
        msg.setObjectName(msg.getDiskName());

        reply.setDiskId(volumeId);
        return reply;
    }
}