package com.gcloud.controller.storage.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.storage.dao.SnapshotDao;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.Snapshot;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.model.DescribeSnapshotsParams;
import com.gcloud.controller.storage.provider.ISnapshotProvider;
import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.rowmapper.GcloudBeanPropertyRowMapper;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.VolumeStatus;
import com.gcloud.header.storage.model.SnapshotType;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Transactional(propagation = Propagation.REQUIRED)
@Slf4j
public class SnapshotServiceImpl implements ISnapshotService {

    @Autowired
    private StoragePoolDao poolDao;

    @Autowired
    private VolumeDao volumeDao;

    @Autowired
    private SnapshotDao snapshotDao;
    
    @Autowired
    private VolumeAttachmentDao volumeAttachmentDao;
    
    @Autowired
    private IVmBaseService vmBaseService;

    @Override
    public PageResult<SnapshotType> describeSnapshots(DescribeSnapshotsParams params, CurrentUser currentUser) {

        if (params == null) {
            params = new DescribeSnapshotsParams();
        }

        String snapshotJsonIds = params.getSnapshotIds();
        if (snapshotJsonIds != null) {
        	List<String> idList = null;
        	try{
        		idList = JSONObject.parseArray(snapshotJsonIds, String.class);
        	} catch(Exception e) {
        		throw new GCloudException("::快照ID传参格式不对，请用JSON数组格式");
        	}
            if (idList == null) {
                throw new GCloudException(StorageErrorCodes.INPUT_SNAPSHOT_ID_ERROR);
            }
            if (idList.size() > 100) {
                throw new GCloudException(StorageErrorCodes.INPUT_SNAPSHOT_ID_ERROR);
            }
            params.setSnapshotIdList(idList);
        }
        PageResult<SnapshotType> result = snapshotDao.describeSnapshots(params, SnapshotType.class, currentUser);
        for(SnapshotType item:result.getList()) {
        	item.setCnStatus(VolumeStatus.getCnName(item.getStatus()));
        	item.setSourceDiskName(CacheContainer.getInstance().getString(CacheType.VOLUME_NAME, item.getSourceDiskId()));
        }

        return result;
    }

    @Override
    public String createSnapshot(String diskId, String name, String description, CurrentUser currentUser, String taskId, String snapshotId) throws GCloudException {
        Volume volume = volumeDao.checkAndGet(diskId);
        StoragePool pool = poolDao.checkAndGet(volume.getPoolId());
        this.getProvider(volume.getProvider()).createSnapshot(pool, volume, snapshotId, name, description, currentUser, taskId);
        return snapshotId;
    }

    @Override
    public void handleCreateSnapshotSuccess(String snapshotId) {
        this.snapshotDao.updateStatus(snapshotId, VolumeStatus.AVAILABLE.value());
        log.info("创建快照成功：{}", snapshotId);
    }

    @Override
    public void handleCreateSnapshotFailed(String errorCode, String snapshotId) {
        this.snapshotDao.deleteById(snapshotId);
        log.info("创建快照失败：{} -> {}", snapshotId, errorCode);
    }

    @Override
    public void updateSnapshot(String id, String name, String description) throws GCloudException {
        Snapshot snapshot = snapshotDao.checkAndGet(id);

        List<String> updateFields = new ArrayList<>();
        Snapshot updateSnap = new Snapshot();
        updateSnap.setId(id);
        updateFields.add(updateSnap.updateDisplayName(name));

        snapshotDao.update(updateSnap, updateFields);
        CacheContainer.getInstance().put(CacheType.SNAPSHOT_NAME, id, name);
        this.getProvider(snapshot.getProvider()).updateSnapshot(snapshot.getProviderRefId(), name, description);
    }

    @Override
    public void deleteSnapshot(String id, String taskId) throws GCloudException {
        Snapshot snapshot = snapshotDao.checkAndGet(id);
        Volume volume = volumeDao.checkAndGet(snapshot.getVolumeId());
        StoragePool pool = poolDao.checkAndGet(volume.getPoolId());
        snapshotDao.updateStatus(id, VolumeStatus.DELETING.value());
        
        this.getProvider(snapshot.getProvider()).deleteSnapshot(pool, volume, snapshot, taskId);
    }

    @Override
    public void handleDeleteSnapshotSuccess(String snapshotId) throws GCloudException {
        this.snapshotDao.deleteById(snapshotId);
        log.info("删除快照成功：{}", snapshotId);
    }

    @Override
    public void handleDeleteSnapshotFailed(String errorCode, String snapshotId) {
        this.snapshotDao.updateStatus(snapshotId, VolumeStatus.AVAILABLE.value());
        log.info("删除快照失败：{} -> {}", snapshotId, errorCode);
    }

    @Override
    public void resetSnapshot(String snapshotId, String diskId, String taskId) throws GCloudException {
        Snapshot snapshot = snapshotDao.checkAndGet(snapshotId);
        Volume volume = volumeDao.checkAndGet(snapshot.getVolumeId());
        //关机或可用状态下才可快照恢复
        List<VolumeAttachment> volumeAttachments = volumeAttachmentDao.findByProperty("volumeId", snapshot.getVolumeId());
        if(volumeAttachments.size() > 0) {
        	VmInstance vm = vmBaseService.getInstanceById(volumeAttachments.get(0).getInstanceUuid());
        	if(!vm.getState().equals(VmState.STOPPED.value())) {
        		throw new GCloudException("::只有在关机或卸载状�?�下才能进行快照恢复");
        	}
        } else if(!volume.getStatus().equals(VolumeStatus.AVAILABLE.value())){
        	throw new GCloudException("::只有在关机或卸载状�?�下才能进行快照恢复");
        }
        
        StoragePool pool = poolDao.checkAndGet(volume.getPoolId());
        this.getProvider(snapshot.getProvider()).resetSnapshot(pool, volume, snapshot, diskId, taskId);
    }

    @Override
    public void handleResetSnapshotSuccess(String snapshotId, List<String> snapshotsToDelete) {
        this.snapshotDao.updateStatus(snapshotId, VolumeStatus.AVAILABLE.value());
        log.info("reset快照成功：{}", snapshotId);
        if (snapshotsToDelete != null) {
            for (String snapshotToDelete : snapshotsToDelete) {
                this.snapshotDao.deleteById(snapshotToDelete);
            }
        }
    }

    @Override
    public void handleResetSnapshotFailed(String errorCode, String snapshotId) {
        this.snapshotDao.updateStatus(snapshotId, VolumeStatus.AVAILABLE.value());
        log.info("reset快照失败：{} -> {}", snapshotId, errorCode);
    }

    private ISnapshotProvider getProvider(int providerType) {
        ISnapshotProvider provider = ResourceProviders.checkAndGet(ResourceType.SNAPSHOT, providerType);
        return provider;
    }

	@Override
	public List<Snapshot> findSnapshotByVolume(String volumeId) {
		return snapshotDao.findByVolume(volumeId);
	}

}