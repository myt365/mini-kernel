package com.gcloud.controller.storage.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.storage.entity.StoragePoolNode;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Jdbc("controllerJdbcTemplate")
@Repository
public class StoragePoolNodeDao extends JdbcBaseDaoImpl<StoragePoolNode, Integer>{
	public List<StoragePoolNode> getStoragePoolNodes(String poolId) {
		List<Object> values = new ArrayList<>();

        StringBuffer sql = new StringBuffer();
        sql.append("select pn.* from gc_storage_pool_nodes pn LEFT JOIN gc_storage_nodes n ON pn.hostname=n.hostname "); 
        sql.append("LEFT JOIN gc_storage_pools p on pn.pool_name=p.pool_name and pn.storage_type=p.storage_type "); 
        sql.append("where p.id=? and n.state='1'");
        values.add(poolId);

        return findBySql(sql.toString(), values, StoragePoolNode.class);
	}
}