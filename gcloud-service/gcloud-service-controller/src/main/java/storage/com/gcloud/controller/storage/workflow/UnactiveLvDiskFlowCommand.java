package com.gcloud.controller.storage.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.storage.workflow.model.volume.UnactiveLvDiskFlowCommandReq;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.storage.msg.node.volume.lvm.NodeUnActiveLvMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class UnactiveLvDiskFlowCommand extends BaseWorkFlowCommand {
	@Autowired
	MessageBus bus;
	
	@Override
	public boolean judgeExecute() {
		UnactiveLvDiskFlowCommandReq req =  (UnactiveLvDiskFlowCommandReq)getReqParams();
		return req.getRepeatParams()!=null?true:false;
	}
	
	@Override
	protected Object process() throws Exception {
		UnactiveLvDiskFlowCommandReq req =  (UnactiveLvDiskFlowCommandReq)getReqParams();
		NodeUnActiveLvMsg msg = new NodeUnActiveLvMsg();
		msg.setLvPath(req.getRepeatParams().getLvPath());
		msg.setServiceId(MessageUtil.storageServiceId(req.getRepeatParams().getHostname()));
        msg.setTaskId(this.getTaskId());
        this.bus.call(msg);
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return UnactiveLvDiskFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}

}