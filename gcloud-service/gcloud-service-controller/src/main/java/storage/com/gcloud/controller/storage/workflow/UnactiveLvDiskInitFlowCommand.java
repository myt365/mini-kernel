package com.gcloud.controller.storage.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.service.IStorageLvNodeService;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.controller.storage.workflow.model.volume.UnactiveLvDiskInitFlowCommandReq;
import com.gcloud.controller.storage.workflow.model.volume.UnactiveLvDiskInitFlowCommandRes;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.service.common.lvm.uitls.LvmUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class UnactiveLvDiskInitFlowCommand extends BaseWorkFlowCommand {
	@Autowired
    private IVolumeService volumeService;
	
	@Autowired
    private IStorageLvNodeService storageLvNodeService;

	@Override
	protected Object process() throws Exception {
		UnactiveLvDiskInitFlowCommandReq req = (UnactiveLvDiskInitFlowCommandReq)getReqParams();
		Volume vol = volumeService.getVolume(req.getVolumeId());
		if(vol == null) {
			throw new GCloudException(":: 该磁盘不存在");
		}
		String lvPath = LvmUtil.getVolumePath(vol.getPoolName(), vol.getId());
		UnactiveLvDiskInitFlowCommandRes res = new UnactiveLvDiskInitFlowCommandRes();
		res.setLvNodes(storageLvNodeService.findByPath(lvPath));
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return UnactiveLvDiskInitFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return UnactiveLvDiskInitFlowCommandRes.class;
	}

}