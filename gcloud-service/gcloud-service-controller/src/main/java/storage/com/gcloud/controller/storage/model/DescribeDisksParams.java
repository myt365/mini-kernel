package com.gcloud.controller.storage.model;

import com.gcloud.common.model.PageParams;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class DescribeDisksParams extends PageParams {

    private String diskType;
    private String status;
    private List<String> statuses;
    private String diskName;
    private String instanceId;
    private List<String> diskIds;
    private String hostname;
    private boolean emptyList;
    private boolean hasSnapshot;

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

	public List<String> getDiskIds() {
		return diskIds;
	}

	public void setDiskIds(List<String> diskIds) {
		this.diskIds = diskIds;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

    public List<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public boolean isEmptyList() {
        return emptyList;
    }

    public void setEmptyList(boolean emptyList) {
        this.emptyList = emptyList;
    }

	public boolean isHasSnapshot() {
		return hasSnapshot;
	}

	public void setHasSnapshot(boolean hasSnapshot) {
		this.hasSnapshot = hasSnapshot;
	}
    
}