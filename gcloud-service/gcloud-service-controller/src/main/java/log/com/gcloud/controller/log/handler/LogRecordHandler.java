package com.gcloud.controller.log.handler;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.log.service.ILogService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.log.LogRecordMsg;
import com.gcloud.header.log.LogRecordReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class LogRecordHandler extends MessageHandler<LogRecordMsg, LogRecordReplyMsg>{
	@Autowired
	private ILogService logService;
	
	@Override
	public LogRecordReplyMsg handle(LogRecordMsg msg) throws GCloudException {
		logService.logRecord(msg);
		return null;
	}

}