package com.gcloud.controller.compute.workflow.vm.create;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.model.vm.CreateInstanceByImageInitParams;
import com.gcloud.controller.compute.model.vm.CreateInstanceByImageInitResponse;
import com.gcloud.controller.compute.service.vm.create.IVmCreateService;
import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceFlowInitCommandReq;
import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceFlowInitCommandRes;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.api.model.DiskInfo;
import com.gcloud.header.storage.enums.DiskType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class CreateInstanceFlowInitCommand extends BaseWorkFlowCommand {

	@Autowired
	private IVmCreateService vmCreateService;
	@Autowired
	private IStoragePoolService storagePoolService;

	@Override
	protected Object process() throws Exception {
		CreateInstanceFlowInitCommandReq req = (CreateInstanceFlowInitCommandReq) getReqParams();
		CreateInstanceByImageInitParams params = BeanUtil.copyProperties(req, CreateInstanceByImageInitParams.class);
		
		if(StringUtils.isBlank(req.getImageId()) && StringUtils.isBlank(req.getIsoId())){
			throw new GCloudException("::镜像ID或映像ID不能同时为空");
		}
		
		CreateInstanceByImageInitResponse createInitRes = new CreateInstanceByImageInitResponse();
		createInitRes =	vmCreateService.createInstanceInit(params, req.getCurrentUser());

		DiskInfo systemDisk = new DiskInfo();
		systemDisk.setDiskName(StringUtils.isNotBlank(req.getSystemDiskName())?req.getSystemDiskName():(req.getInstanceName() + "_系统�?"));
		systemDisk.setCategory(req.getSystemDiskCategory());
		systemDisk.setDiskType(DiskType.SYSTEM);
		systemDisk.setImageRef(req.getImageId());
		systemDisk.setZoneId(req.getZoneId());
		systemDisk.setCreateHost(createInitRes.getCreateHost());
		StoragePool sysStoragePool = storagePoolService.assignStoragePool(systemDisk.getCategory(), systemDisk.getZoneId(), systemDisk.getCreateHost());
		systemDisk.setPoolId(sysStoragePool.getId());

		CreateInstanceFlowInitCommandRes res = new CreateInstanceFlowInitCommandRes();
		systemDisk.setSize(createInitRes.getSystemDiskSize());
		res.setStorageType(createInitRes.getStorageType());
		res.setImageInfo(createInitRes.getImageInfo());
		res.setCreateHost(createInitRes.getCreateHost());
		res.setInstanceId(createInitRes.getId());
		res.setCpu(createInitRes.getCpu());
		res.setMemory(createInitRes.getMemory());
		res.setSystemDiskSize(createInitRes.getSystemDiskSize());
		res.setCreateUser(createInitRes.getCreateUser());

		res.setInstanceName(req.getInstanceName());
		res.setSystemDisk(systemDisk);
		res.setIsoId(req.getIsoId());
		res.setIsoCreate(StringUtils.isBlank(req.getImageId()) && StringUtils.isNotBlank(req.getIsoId()));

		if(req.getDataDisk() != null && req.getDataDisk().size() > 0){
			int i = 0;
			for(DiskInfo diskInfo : req.getDataDisk()){
				i++;
				if(StringUtils.isBlank(diskInfo.getZoneId())){
					diskInfo.setZoneId(req.getZoneId());
				}
				if(StringUtils.isBlank(diskInfo.getDiskName())){
					diskInfo.setDiskName(req.getInstanceName() + "_数据�?" + i);
				}
				diskInfo.setCreateHost(createInitRes.getCreateHost());

			}
		}
		res.setDataDisk(req.getDataDisk());
		if(StringUtils.isNotBlank(req.getSubnetId())) {
			res.setPortName(req.getInstanceName() + "_网卡");
		}


		//如果有其他业务�?�辑，跑错时�?要调用rollback

		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		CreateInstanceFlowInitCommandRes res = (CreateInstanceFlowInitCommandRes)getResParams();
	    try{
            InstanceDao instanceDao = (InstanceDao) SpringUtil.getBean("instanceDao");
            instanceDao.deleteById(res.getInstanceId());
        }catch (Exception ex){
            log.error("创建云服务器回滚，删除虚拟机失败", ex);
        }


        try{
            Dispatcher.dispatcher().release(res.getCreateHost(), res.getCpu(), res.getMemory());
        }catch (Exception ex){
            log.error("创建云服务器回滚，释放资源失�?", ex);
        }

		return null;
	}


	@Override
	protected Object timeout() throws Exception {
		CreateInstanceFlowInitCommandRes res = (CreateInstanceFlowInitCommandRes) getResParams();
		InstanceDao instanceDao = (InstanceDao) SpringUtil.getBean("instanceDao");
		/*
		 * if(vm.getState().equals(VmState)) {
		 * 
		 * }
		 */
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CreateInstanceFlowInitCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return CreateInstanceFlowInitCommandRes.class;
	}

}