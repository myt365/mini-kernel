package com.gcloud.controller.compute.model.node;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public class ResourceUnit {
	private int core;
	private int memory;

	public int getCore() {
		return core;
	}

	public int getMemory() {
		return memory;
	}

	public void addCore(int core) {
		this.core += core;
	}

	public void addMemory(int memory) {
		this.memory += memory;
	}

	public void setCore(int core) {
		this.core = core;
	}

	public void setMemory(int memory) {
		this.memory = memory;
	}
}