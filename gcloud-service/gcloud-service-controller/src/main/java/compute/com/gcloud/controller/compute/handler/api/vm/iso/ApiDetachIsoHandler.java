package com.gcloud.controller.compute.handler.api.vm.iso;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.service.vm.iso.IVmIsoService;
import com.gcloud.controller.compute.workflow.model.iso.DetachIsoInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.iso.DetachIsoWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.iso.DetachIsoWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.compute.msg.api.vm.iso.ApiDetachIsoMsg;
import com.gcloud.header.compute.msg.api.vm.iso.ApiDetachIsoReplyMsg;
import com.gcloud.header.log.model.Task;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@LongTask
@GcLog(isMultiLog = true, taskExpect = "卸载光盘")
@ApiHandler(module = Module.ECS, action = "DetachIso",name="卸载光盘")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class ApiDetachIsoHandler extends BaseWorkFlowHandler<ApiDetachIsoMsg, ApiDetachIsoReplyMsg>{
	@Autowired
	IVmIsoService vmIsoService;
	
	@Override
	public Object initParams(ApiDetachIsoMsg msg) {
		DetachIsoWorkflowReq req = new DetachIsoWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setIsoId(msg.getIsoId());
        req.setInTask(false);
		return req;
	}

	@Override
	public Object preProcess(ApiDetachIsoMsg msg) throws GCloudException {
		vmIsoService.detachIsoApiCheck(msg.getIsoId());
		return null;
	}

	@Override
	public ApiDetachIsoReplyMsg process(ApiDetachIsoMsg msg) throws GCloudException {
		ApiDetachIsoReplyMsg reply = new ApiDetachIsoReplyMsg();
		// 记录操作日志，任务流无论单操作还是批�? 都用这种方式记录操作日志
		DetachIsoInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DetachIsoInitFlowCommandRes.class);
		String except = String.format("卸载光盘[%s]成功", res.getIsoId());
		reply.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(res.getIsoId())
				.objectName(CacheContainer.getInstance().getString(CacheType.ISO_NAME, msg.getIsoId())).expect(except).build());

		String requestId = UUID.randomUUID().toString();
		reply.setRequestId(requestId);
		reply.setSuccess(true);
		return reply;
	}

	@Override
	public Class getWorkflowClass() {
		return DetachIsoWorkflow.class;
	}

}