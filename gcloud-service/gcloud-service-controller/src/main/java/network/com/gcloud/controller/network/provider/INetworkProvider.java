package com.gcloud.controller.network.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.model.CreateNetworkParams;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.network.msg.api.CreateExternalNetworkMsg;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface INetworkProvider extends IResourceProvider {

    void createNetwork(String networkId, CreateNetworkParams msg, CurrentUser currentUser);
    
    void createExternalNetwork(String networkId, CreateExternalNetworkMsg msg);

    void removeNetwork(String vpcRefId);

    void updateNetwork(String vpcRefId, String vpcName);

    List<Network> list(Map<String, String> filter);
}