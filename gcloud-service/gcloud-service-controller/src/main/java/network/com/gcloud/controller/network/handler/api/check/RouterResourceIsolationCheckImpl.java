package com.gcloud.controller.network.handler.api.check;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.network.dao.RouterDao;
import com.gcloud.controller.network.entity.Router;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationType;
import com.gcloud.core.currentUser.policy.service.ResourceIsolationCheckImpl;
import com.gcloud.header.common.ResourceOwner;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
public class RouterResourceIsolationCheckImpl extends ResourceIsolationCheckImpl {
	@Autowired
    private RouterDao routerDao;
	
	@Override
	public ResourceOwner getResourceTenantId(String resourceId) {
		Router router = routerDao.getById(resourceId);
		ResourceOwner resourceOwner = null;
		if(router != null) {
			resourceOwner = new ResourceOwner(router.getUserId(), router.getTenantId());
		}
		return resourceOwner;
	}
	
	@Override
	public ResourceIsolationType getIsolationType() {
		return ResourceIsolationType.IN_TENANT;
	}

}