package com.gcloud.controller.network.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.network.entity.SecurityGroupRule;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Repository
public class SecurityGroupRuleDao extends JdbcBaseDaoImpl<SecurityGroupRule, String>{
	public void deleteBySecurityGroup(String securityGroupId) {
		String sql = "delete from gc_security_group_rules ";
		sql += "where security_group_id='" + securityGroupId + "' ";
		
	    this.jdbcTemplate.execute(sql);
	}
}