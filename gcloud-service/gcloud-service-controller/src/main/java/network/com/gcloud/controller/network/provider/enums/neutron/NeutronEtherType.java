package com.gcloud.controller.network.provider.enums.neutron;


import com.gcloud.header.compute.enums.EtherType;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum NeutronEtherType {
    IPv4("IPv4", EtherType.IPv4),
    IPv6("IPv6", EtherType.IPv6);

    NeutronEtherType(String neutronEtherType, EtherType etherType) {
        this.neutronEtherType = neutronEtherType;
        this.etherType = etherType;
    }

    private String neutronEtherType;
    private EtherType etherType;

    public String getNeutronEtherType() {
        return neutronEtherType;
    }

    public EtherType getEtherType() {
        return etherType;
    }

    public static String toGCloudValue(String neutronValue){
        NeutronEtherType type = Arrays.stream(NeutronEtherType.values()).filter(o -> o.getNeutronEtherType().equals(neutronValue)).findFirst().orElse(null);
        return type == null ? null : type.getEtherType().getValue();
    }

    public static String getByGcValue(String gcValue){
        NeutronEtherType type = Arrays.stream(NeutronEtherType.values()).filter(o -> o.getEtherType().getValue().equals(gcValue)).findFirst().orElse(null);
        return type == null ? null : type.getNeutronEtherType();
    }
}