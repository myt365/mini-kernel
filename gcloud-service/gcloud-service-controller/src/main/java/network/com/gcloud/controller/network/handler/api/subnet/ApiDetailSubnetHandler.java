package com.gcloud.controller.network.handler.api.subnet;

import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DetailSubnet;
import com.gcloud.header.network.msg.api.ApiDetailSubnetMsg;
import com.gcloud.header.network.msg.api.ApiDetailSubnetReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.SUBNET,action="DetailSubnet",name="子网详情")
public class ApiDetailSubnetHandler extends MessageHandler<ApiDetailSubnetMsg, ApiDetailSubnetReplyMsg>{

	@Autowired
	private ISubnetService subnetService;

	@Override
	public ApiDetailSubnetReplyMsg handle(ApiDetailSubnetMsg msg) throws GCloudException {

		DetailSubnet subnet = subnetService.detail(msg.getSubnetId());
		ApiDetailSubnetReplyMsg reply = new ApiDetailSubnetReplyMsg();
		reply.setDetailSubnet(subnet);
		return reply ;
	}
}