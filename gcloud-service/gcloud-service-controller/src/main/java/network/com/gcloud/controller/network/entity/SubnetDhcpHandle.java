package com.gcloud.controller.network.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Table(name = "gc_subnet_dhcp_handles", jdbc = "controllerJdbcTemplate")
public class SubnetDhcpHandle {

    @ID
    private String subnetId;
    private String handleId;//uuid，有就是�?要进行处�?
    private Boolean deleteSubnet; //是否进行删除子网操作
    private Integer retry;
    private Date updateTime;

    public static final String SUBNET_ID = "subnetId";
    public static final String HANDLE_ID = "handleId";
    public static final String DELETE_SUBNET = "deleteSubnet";
    public static final String RETRY = "retry";
    public static final String UPDATE_TIME = "updateTime";

    public String getSubnetId() {
        return subnetId;
    }

    public void setSubnetId(String subnetId) {
        this.subnetId = subnetId;
    }

    public String getHandleId() {
        return handleId;
    }

    public void setHandleId(String handleId) {
        this.handleId = handleId;
    }

    public Boolean getDeleteSubnet() {
        return deleteSubnet;
    }

    public void setDeleteSubnet(Boolean deleteSubnet) {
        this.deleteSubnet = deleteSubnet;
    }

    public Integer getRetry() {
        return retry;
    }

    public void setRetry(Integer retry) {
        this.retry = retry;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String updateSubnetId (String subnetId){
        this.setSubnetId(subnetId);
        return SUBNET_ID;
    }
    public String updateHandleId (String handleId){
        this.setHandleId(handleId);
        return HANDLE_ID;
    }
    public String updateDeleteSubnet (Boolean deleteSubnet){
        this.setDeleteSubnet(deleteSubnet);
        return DELETE_SUBNET;
    }
    public String updateRetry (Integer retry){
        this.setRetry(retry);
        return RETRY;
    }
    public String updateUpdateTime (Date updateTime){
        this.setUpdateTime(updateTime);
        return UPDATE_TIME;
    }
}