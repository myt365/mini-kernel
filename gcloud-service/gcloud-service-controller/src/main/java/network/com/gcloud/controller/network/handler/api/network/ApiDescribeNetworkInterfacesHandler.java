package com.gcloud.controller.network.handler.api.network;

import com.gcloud.controller.network.model.DescribeNetworkInterfacesParams;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.NetworkInterfaceSet;
import com.gcloud.header.network.msg.api.DescribeNetworkInterfacesMsg;
import com.gcloud.header.network.msg.api.DescribeNetworkInterfacesReplyMsg;
import com.gcloud.header.storage.model.DiskItemType;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.NETWORKINTERFACE,action="DescribeNetworkInterfaces",name="网卡列表")
public class ApiDescribeNetworkInterfacesHandler extends MessageHandler<DescribeNetworkInterfacesMsg, DescribeNetworkInterfacesReplyMsg> {

	@Autowired
	private IPortService portService;

	@Override
	public DescribeNetworkInterfacesReplyMsg handle(DescribeNetworkInterfacesMsg msg) throws GCloudException {

		DescribeNetworkInterfacesParams params = BeanUtil.copyBean(msg, DescribeNetworkInterfacesParams.class);
		PageResult<NetworkInterfaceSet> response = portService.describe(params, msg.getCurrentUser());
		DescribeNetworkInterfacesReplyMsg replyMsg = new DescribeNetworkInterfacesReplyMsg();
		replyMsg.init(response);
		return replyMsg;
	}

}