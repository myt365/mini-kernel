package com.gcloud.controller.network.handler.api.network;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IRouterService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ApiRoutersStatisticsMsg;
import com.gcloud.header.network.msg.api.ApiRoutersStatisticsReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@ApiHandler(module = Module.ECS,subModule=SubModule.VPC,action = "VpcStatistics",name="虚拟私有云统�?")

public class ApiVpcsStatisticsHandler  extends MessageHandler<ApiRoutersStatisticsMsg, ApiRoutersStatisticsReplyMsg>{
	@Autowired
	private IRouterService routerService;

	@Override
	public ApiRoutersStatisticsReplyMsg handle(ApiRoutersStatisticsMsg msg) throws GCloudException {
		ApiRoutersStatisticsReplyMsg replyMsg = new ApiRoutersStatisticsReplyMsg();
		replyMsg.setAllNum(routerService.routerStatistics(msg.getCurrentUser()));
		return replyMsg;
	}
}