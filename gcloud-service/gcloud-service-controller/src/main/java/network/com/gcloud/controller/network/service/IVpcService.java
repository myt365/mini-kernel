package com.gcloud.controller.network.service;

import com.gcloud.framework.db.PageResult;
import com.gcloud.header.network.model.DetailVpcResponse;
import com.gcloud.header.network.model.TplVpcResponse;
import com.gcloud.header.network.model.VpcsItemType;
import com.gcloud.header.network.msg.api.ApiTplVpcsMsg;
import com.gcloud.header.network.msg.api.CreateVpcMsg;
import com.gcloud.header.network.msg.api.DescribeVpcsMsg;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IVpcService {

	PageResult<VpcsItemType> describeVpcs(DescribeVpcsMsg msg);
	String createVpc(CreateVpcMsg msg);
	void removeVpc(String vpcId);
	void updateVpc(ModifyVpcAttributeMsg msg);
	DetailVpcResponse detail(String vpcId);
	TplVpcResponse tplVpcs(ApiTplVpcsMsg msg);
}