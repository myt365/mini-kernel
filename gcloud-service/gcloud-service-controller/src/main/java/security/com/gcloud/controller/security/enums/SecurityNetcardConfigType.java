package com.gcloud.controller.security.enums;

import com.google.common.base.CaseFormat;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum SecurityNetcardConfigType {

    NORMAL("normal"),OVS_BRIDGE("ovs-bridge");

    private String value;

    SecurityNetcardConfigType(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public static SecurityNetcardConfigType getByValue(String value){
        return Arrays.stream(SecurityNetcardConfigType.values()).filter(s -> s.value().equals(value)).findFirst().orElse(null);
    }
}