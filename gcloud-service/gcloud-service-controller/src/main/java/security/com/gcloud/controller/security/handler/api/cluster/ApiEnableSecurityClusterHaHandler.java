package com.gcloud.controller.security.handler.api.cluster;

import com.gcloud.controller.security.model.workflow.EnableSecurityClusterHaInitFlowCommandRes;
import com.gcloud.controller.security.workflow.cluster.EnableSecurityClusterHaWorkflow;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.log.model.Task;
import com.gcloud.header.security.msg.api.cluster.ApiEnableSecurityClusterHaMsg;
import com.gcloud.header.security.msg.api.cluster.ApiEnableSecurityClusterHaReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.SECURITYCLUSTER, action = "EnableSecurityClusterHa", name = "启用HA")
@LongTask
@GcLog(taskExpect = "启用HA")
public class ApiEnableSecurityClusterHaHandler extends BaseWorkFlowHandler<ApiEnableSecurityClusterHaMsg, ApiEnableSecurityClusterHaReplyMsg> {

    @Override
    public Object preProcess(ApiEnableSecurityClusterHaMsg msg) throws GCloudException {
        return null;
    }

    @Override
    public ApiEnableSecurityClusterHaReplyMsg process(ApiEnableSecurityClusterHaMsg msg) throws GCloudException {

        ApiEnableSecurityClusterHaReplyMsg replyMessage = new ApiEnableSecurityClusterHaReplyMsg();
        EnableSecurityClusterHaInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), EnableSecurityClusterHaInitFlowCommandRes.class);

        msg.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(res.getClusterId()).expect("安全集群启用HA成功").build());

        replyMessage.setTaskId(res.getTaskId());
        return replyMessage;

    }

    @Override
    public Class getWorkflowClass() {
        return EnableSecurityClusterHaWorkflow.class;
    }
}