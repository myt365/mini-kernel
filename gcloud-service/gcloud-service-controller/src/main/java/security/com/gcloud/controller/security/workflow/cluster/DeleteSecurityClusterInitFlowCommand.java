package com.gcloud.controller.security.workflow.cluster;

import com.gcloud.controller.security.dao.SecurityClusterComponentDao;
import com.gcloud.controller.security.dao.SecurityClusterOvsBridgeDao;
import com.gcloud.controller.security.entity.SecurityClusterComponent;
import com.gcloud.controller.security.entity.SecurityClusterOvsBridge;
import com.gcloud.controller.security.model.workflow.DeleteSecurityClusterInitFlowCommandReq;
import com.gcloud.controller.security.model.workflow.DeleteSecurityClusterInitFlowCommandRes;
import com.gcloud.controller.security.model.workflow.SecurityClusterComponentInfo;
import com.gcloud.controller.security.model.workflow.SecurityClusterOvsBridgeInfo;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class DeleteSecurityClusterInitFlowCommand extends BaseWorkFlowCommand {

    @Autowired
    private SecurityClusterComponentDao clusterComponentDao;

    @Autowired
    private SecurityClusterOvsBridgeDao clusterOvsBridgeDao;

    @Autowired
    private ISecurityClusterService securityClusterService;

    @Override
    protected Object process() throws Exception {

        DeleteSecurityClusterInitFlowCommandReq req = (DeleteSecurityClusterInitFlowCommandReq)getReqParams();

        securityClusterService.delete(req.getClusterId());

        List<SecurityClusterComponentInfo> components = clusterComponentDao.findByProperty(SecurityClusterComponent.CLUSTER_ID, req.getClusterId(), SecurityClusterComponentInfo.class);
        List<SecurityClusterOvsBridgeInfo> ovsBridges = clusterOvsBridgeDao.findByProperty(SecurityClusterOvsBridge.CLUSTER_ID, req.getClusterId(), SecurityClusterOvsBridgeInfo.class);

        DeleteSecurityClusterInitFlowCommandRes res = new DeleteSecurityClusterInitFlowCommandRes();
        res.setComponents(components);
        res.setOvsBridges(ovsBridges);

        return res;
    }

    @Override
    protected Object rollback() throws Exception {
        return null;
    }

    @Override
    protected Object timeout() throws Exception {
        return null;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return DeleteSecurityClusterInitFlowCommandReq.class;
    }

    @Override
    protected Class<?> getResParamClass() {
        return DeleteSecurityClusterInitFlowCommandRes.class;
    }
}