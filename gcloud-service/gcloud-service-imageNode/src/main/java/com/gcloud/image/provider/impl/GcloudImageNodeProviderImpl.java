package com.gcloud.image.provider.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.image.msg.node.CheckImageStoreMsg;
import com.gcloud.header.image.msg.node.CheckImageStoreReplyMsg;
import com.gcloud.header.image.msg.node.DownloadImageMsg;
import com.gcloud.image.driver.IImageStoreNodeDriver;
import com.gcloud.image.driver.ImageDriverNodeEnum;
import com.gcloud.image.provider.IImageProvider;
import com.gcloud.service.common.enums.DistributeAction;
import com.gcloud.service.common.enums.ImageStoreStatus;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Slf4j
public class GcloudImageNodeProviderImpl implements IImageProvider {
	@Autowired
	private MessageBus bus;
	
	public void downloadImage(DownloadImageMsg msg) {
		IImageStoreNodeDriver driver = (IImageStoreNodeDriver)ImageDriverNodeEnum.getByType(msg.getImageStroageType(), msg.getTargetType());
		if(driver == null) {
			throw new GCloudException("::该类型驱动不存在");
		}
		if(msg.getDistributeAction().equals(DistributeAction.DISTRIBUTE.value())) {
			driver.downloadImage(msg.getImagePath(), msg.getImageId(), msg.getTarget(), msg.getImageResourceType());
		}else {//check
			if(!checkImage(msg)) {
				throw new GCloudException("::镜像分发失败");
			}
		}
	}
	
	private boolean checkImage(DownloadImageMsg msg) {
		int times = 30;
		long sleepTime = 60000;//1分钟
		for (int i = 1; i <= times; i++) {
			try {
				// �?�?
				log.debug(String.format("镜像/映像%s�?%s次分发检测开�?", msg.getImageId(), i));
				
				CheckImageStoreMsg checkMsg = new CheckImageStoreMsg();
				checkMsg.setImageId(msg.getImageId());
				checkMsg.setImageResourceType(msg.getImageResourceType());
				checkMsg.setTarget(msg.getTarget());
				checkMsg.setTargetType(msg.getTargetType());
				checkMsg.setServiceId(MessageUtil.controllerServiceId());
				CheckImageStoreReplyMsg reply = bus.call(checkMsg, CheckImageStoreReplyMsg.class);
				if(reply.getStatus().equals(ImageStoreStatus.ACTIVE.value())) {
					log.debug(String.format("镜像/映像%s分发�?测成�?", msg.getImageId()));
					return true;
				}else if(StringUtils.isBlank(reply.getStatus())) {
					//throw new GCloudException(String.format("镜像/映像%s分发�?测失�?", msg.getImageId()));
					return false;
				}
				
				log.debug(String.format("镜像/映像%s�?%s次分发检测结�?", msg.getImageId(), i));
			} catch (Exception exx) {
				log.error("镜像/映像�?测失�?", exx);
				log.debug(String.format("镜像/映像%s�?%s次分发检测失�?", msg.getImageId(), i));
				if (i == times) {
					if (exx instanceof GCloudException) {
						throw (GCloudException) exx;
					} else {
						throw new GCloudException(String.format("镜像/映像%s分发�?测失�?", msg.getImageId()));
					}

				}
			}
			if (i < times) {
				try {
					Thread.sleep(sleepTime);
				} catch (Exception exb) {
					log.error(String.format("镜像/映像%s分发�?测失�?", msg.getImageId()), exb);
				}
			}
		}
		return false;
	}

}