package com.gcloud.compute.handler.vm.base;

import com.gcloud.compute.virtual.IVmVirtual;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.compute.msg.node.vm.base.InstanceDomainDetailMsg;
import com.gcloud.header.compute.msg.node.vm.base.InstanceDomainDetailReplyMsg;
import com.gcloud.service.common.compute.model.DomainDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
@Slf4j
public class InstanceDomainDetailHandler extends MessageHandler<InstanceDomainDetailMsg, InstanceDomainDetailReplyMsg> {

	@Autowired
	private IVmVirtual vmVirtual;

	@Override
	public InstanceDomainDetailReplyMsg handle(InstanceDomainDetailMsg msg) throws GCloudException {
		InstanceDomainDetailReplyMsg reply = msg.deriveMsg(InstanceDomainDetailReplyMsg.class);
		DomainDetail detail = vmVirtual.getVmDetail(msg.getInstanceId());
		if(detail != null){
			reply.setCurrentMemory(detail.getCurrentMemory());
			reply.setMemory(detail.getMemory());
			reply.setVcpu(detail.getVcpu());
		}
		return reply;
	}
}