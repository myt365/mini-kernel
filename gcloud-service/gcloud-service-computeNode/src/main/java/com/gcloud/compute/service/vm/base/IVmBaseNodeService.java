package com.gcloud.compute.service.vm.base;

import com.gcloud.core.exception.GCloudException;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IVmBaseNodeService {

	void startup(String instanceId);

	void startupDesktop(String instanceId);

	void reboot(String instanceId, Boolean forceStop);

	void stop(String instanceId);

	String vmGcloudState(String instanceId);

	void destroy(String instanceId);

	void destroyIfExist(String instanceId);

	void configInstanceResource(String instanceId, Integer cpu, Integer memory, Integer orgCpu, Integer orgMemory);
	
	void changePassword(String instanceId,String loginName, String password) throws GCloudException;

	String queryVnc(String instanceId);
	
	void changeHostName(String instanceId, String hostname) throws GCloudException;
	
	Boolean checkVmReady(String instanceId, int timeout) throws GCloudException;
	
	boolean fsFreeze(String instanceId, String fsFreezeType);
}