package com.gcloud.boot.config;

import java.util.Random;
import java.util.UUID;

import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.env.StandardEnvironment;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class RandomUUIDPropertySource extends PropertySource<Random>{
	public static final String RANDOM_PROPERTY_SOURCE_NAME = "randomUUID";
	private static final String PREFIX = "randomUUID";
	private static String uuid;
	public RandomUUIDPropertySource(String name) {
		super(name, new Random());
		// TODO Auto-generated constructor stub
	}
	public RandomUUIDPropertySource() {
		this(RANDOM_PROPERTY_SOURCE_NAME);
	}
	
	@Override
	public Object getProperty(String name) {
		// TODO Auto-generated method stub
		if (!name.startsWith(PREFIX)) {
			return null;
		}
		if(uuid==null)
			uuid=UUID.randomUUID().toString().replaceAll("-", "");
		return uuid;
	}
	
	public static void addToEnvironment(ConfigurableEnvironment environment) {
		environment.getPropertySources().addAfter(
				StandardEnvironment.SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME,
				new RandomUUIDPropertySource(RANDOM_PROPERTY_SOURCE_NAME));
	}

}